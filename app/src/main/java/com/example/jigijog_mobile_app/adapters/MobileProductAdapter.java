package com.example.jigijog_mobile_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Kids;
import com.example.jigijog_mobile_app.models.Mobile;

import java.util.ArrayList;

public class MobileProductAdapter extends RecyclerView.Adapter<MobileProductAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Mobile> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public MobileProductAdapter(Context context, ArrayList<Mobile> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_mobile_products, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Mobile mobileModel = mList.get(position);
        ImageView imageView = holder.iv_mobileImage;

        TextView textView1, textView2,textView3;

        textView1 = holder.tv_mobilePrice;
        textView2 = holder.tv_mobileDisPrice;
        textView3 = holder.tv_mobileItemName;
        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + mobileModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_mobileImage);
        double unit_price =  Double.parseDouble(mobileModel.getUnit_price());
        double discount;
        if (mobileModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(mobileModel.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText(String.valueOf(total));
        textView2.setText(mobileModel.getDiscount());
        if (mobileModel.getName().length() <= 24){
            textView3.setText(mobileModel.getName());
        }else{
            textView3.setText(mobileModel.getName().substring(0, 24) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_mobileImage;
        TextView tv_mobilePrice, tv_mobileDisPrice, tv_mobileItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_mobileImage = (ImageView) itemView.findViewById(R.id.iv_mobileImage);
            tv_mobilePrice = (TextView) itemView.findViewById(R.id.tv_mobilePrice);
            tv_mobileDisPrice = (TextView) itemView.findViewById(R.id.tv_mobileDisPrice);
            tv_mobileItemName = (TextView) itemView.findViewById(R.id.tv_mobileItemName);

            iv_mobileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
