package com.example.jigijog_mobile_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Home;

import java.util.ArrayList;

public class HomeProductsGridAdapter extends RecyclerView.Adapter<HomeProductsGridAdapter.ViewHolder>  {
    private Context mContext;
    private ArrayList<Home> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public HomeProductsGridAdapter(Context context, ArrayList<Home> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_health_products_grid, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Home homeModel = mList.get(position);
        ImageView imageView = holder.iv_healthImage;

        TextView textView1, textView2,textView3;

        textView1 = holder.tv_healthPrice;
        textView2 = holder.tv_healthDisPrice;
        textView3 = holder.tv_healthItemName;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + homeModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_healthImage);
        double unit_price =  Double.parseDouble(homeModel.getUnit_price());
        double discount;
        if (homeModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(homeModel.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText(String.valueOf(total));
        textView2.setText(homeModel.getDiscount());
        if (homeModel.getName().length() <= 24){
            textView3.setText(homeModel.getName());
        }else{
            textView3.setText(homeModel.getName().substring(0, 24) + ".." );
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_healthImage;
        TextView tv_healthPrice, tv_healthDisPrice, tv_healthItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_healthImage = (ImageView) itemView.findViewById(R.id.iv_healthImage);
            tv_healthPrice = (TextView) itemView.findViewById(R.id.tv_healthPrice);
            tv_healthDisPrice = (TextView) itemView.findViewById(R.id.tv_healthDisPrice);
            tv_healthItemName = (TextView) itemView.findViewById(R.id.tv_healthItemName);

            iv_healthImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
