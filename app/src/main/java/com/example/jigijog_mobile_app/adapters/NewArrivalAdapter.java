package com.example.jigijog_mobile_app.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.NewArrival;
import com.example.jigijog_mobile_app.models.Sports;

import java.util.ArrayList;

public class NewArrivalAdapter extends RecyclerView.Adapter<NewArrivalAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<NewArrival> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public NewArrivalAdapter(Context context, ArrayList<NewArrival> list) {
        mContext = context;
        mList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_new_arrival, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final NewArrival newArrivalModel = mList.get(position);
        ImageView imageView = holder.iv_newArrivalImage;

        TextView textView1, textView2,textView3;

        textView1 = holder.tv_newArrivalPrice;
        textView2 = holder.tv_newArrivalDisPrice;
        textView3 = holder.tv_newArrivalItemName;

        Glide.with(mContext)
                .load( "https://www.jigijog.com/public/" + newArrivalModel.getFeatured_img())
                .placeholder(R.drawable.ic_account_box_black_24dp)
                .into(holder.iv_newArrivalImage);
        double unit_price =  Double.parseDouble(newArrivalModel.getUnit_price());
        double discount;
        if (newArrivalModel.getDiscount().equals("null")){
            discount = 0.00;
        } else {
            discount =  Double.parseDouble(newArrivalModel.getDiscount());
        }
        double total = unit_price - discount;

        textView1.setText("\u20B1" + " " + String.valueOf(total) + "0");
        textView2.setText("\u20B1" + " " + newArrivalModel.getDiscount());
        textView2.setPaintFlags(textView2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        if (newArrivalModel.getName().length() <= 14){
            textView3.setText(newArrivalModel.getName());
        }else{
            textView3.setText(newArrivalModel.getName().substring(0, 14) + ".." );
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_newArrivalImage;
        TextView tv_newArrivalPrice, tv_newArrivalDisPrice, tv_newArrivalItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_newArrivalImage = (ImageView) itemView.findViewById(R.id.iv_newArrivalImage);
            tv_newArrivalPrice = (TextView) itemView.findViewById(R.id.tv_newArrivalPrice);
            tv_newArrivalDisPrice = (TextView) itemView.findViewById(R.id.tv_newArrivalDisPrice);
            tv_newArrivalItemName = (TextView) itemView.findViewById(R.id.tv_newArrivalItemName);

            iv_newArrivalImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
