package com.example.jigijog_mobile_app.adapters;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Messages;

import java.util.ArrayList;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Messages> mList;
    private OnClickRecyclerView onClickRecyclerView;

    public MessagesAdapter(Context context, ArrayList<Messages> list) {
        mContext = context;
        mList = list;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_messages, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Messages messagesModel = mList.get(position);
        ImageView imageView = holder.messageIcon;
        ImageView imageView1 = holder.messageImage;
        TextView textView1, textView2, textView3;

        textView1 = holder.messageTitle;
        textView2 = holder.messageDate;
        textView3 = holder.messageDescription;

        imageView.setImageResource(messagesModel.getMessageIcon());
        imageView1.setImageResource(messagesModel.getMessageImage());

        textView1.setText(messagesModel.getMessageTitle());
        textView2.setText(messagesModel.getMessageDate());
        textView3.setText(messagesModel.getMessageDescription());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView messageIcon, messageImage;
        private TextView messageTitle, messageDate, messageDescription;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            messageIcon = (ImageView) itemView.findViewById(R.id.iv_messageIcon);
            messageTitle = (TextView) itemView.findViewById(R.id.tv_messageTitle);
            messageDate = (TextView) itemView.findViewById(R.id.tv_messageDate);
            messageDescription = (TextView) itemView.findViewById(R.id.tv_messageDescription);
            messageImage = (ImageView) itemView.findViewById(R.id.iv_messageImage);

            messageImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onClickRecyclerView != null)
                        onClickRecyclerView.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setClickListener(OnClickRecyclerView onClickRecyclerView) {
        this.onClickRecyclerView = onClickRecyclerView;
    }
}
