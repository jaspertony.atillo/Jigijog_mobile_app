package com.example.jigijog_mobile_app.models;

public class FlashDeal {
    private String id;
    private String flash_deal_id;
    private String product_id;
    private String discount;
    private String discount_type;
    private String created_at;
    private String updated_at;

    public FlashDeal(String id, String flash_deal_id, String product_id, String discount, String discount_type, String created_at, String updated_at) {
        this.id = id;
        this.flash_deal_id = flash_deal_id;
        this.product_id = product_id;
        this.discount = discount;
        this.discount_type = discount_type;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public FlashDeal() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlash_deal_id() {
        return flash_deal_id;
    }

    public void setFlash_deal_id(String flash_deal_id) {
        this.flash_deal_id = flash_deal_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount_type() {
        return discount_type;
    }

    public void setDiscount_type(String discount_type) {
        this.discount_type = discount_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
