package com.example.jigijog_mobile_app.models;

public class Messages {
    private int messageIcon;
    private String messageTitle;
    private String messageDate;
    private int messageImage;
    private String messageDescription;

    public Messages(int messageIcon, String messageTitle, String messageDate, int messageImage, String messageDescription) {
        this.messageIcon = messageIcon;
        this.messageTitle = messageTitle;
        this.messageDate = messageDate;
        this.messageImage = messageImage;
        this.messageDescription = messageDescription;
    }

    public Messages() {
    }

    public int getMessageIcon() {
        return messageIcon;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public String getMessageDate() {
        return messageDate;
    }

    public int getMessageImage() {
        return messageImage;
    }

    public String getMessageDescription() {
        return messageDescription;
    }

    public void setMessageIcon(int messageIcon) {
        this.messageIcon = messageIcon;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public void setMessageDate(String messageDate) {
        this.messageDate = messageDate;
    }

    public void setMessageImage(int messageImage) {
        this.messageImage = messageImage;
    }

    public void setMessageDescription(String messageDescription) {
        this.messageDescription = messageDescription;
    }
}
