package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.utils.Tools;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        Tools.setSystemBarColor(this, R.color.deep_orange_400);
        Tools.setSystemBarLight(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(StartActivity.this, SplashActivity.class);
                startActivity(i);
                finish();
            }
        }, 3000);
    }
}
