package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.jigijog_mobile_app.HttpProvider;
import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.SportsProductAdapter;
import com.example.jigijog_mobile_app.adapters.SportsProductGridAdapter;
import com.example.jigijog_mobile_app.adapters.SubCategoriesAdapter;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Sports;
import com.example.jigijog_mobile_app.models.SubCategories;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class ProductCategoryActivity extends AppCompatActivity {
    private View view;
    private Context context;
    private String subcategoryName;
    private String subcategory;
    private int subcategoryID = 0;

    private ImageView iv_productcategoryBack, iv_productcategoySearch, iv_productcategoryCart, iv_productcategoryMore, iv_productcategoryGrid, iv_productcategoryNewest;
    private TextView tv_productcategoryName,tv_productcategoryCart;

    private SwipeRefreshLayout productswipe_Home;
    private RecyclerView productcategoryRecyclerView;

    private ArrayList<Sports> sportsArrayList;
    private SportsProductAdapter sportsProductAdapter;
    private SportsProductGridAdapter sportsProductGridAdapter;

    private Sports selectedSports = new Sports();
    private ConstraintLayout emptyIndicator;

    private int cartCount = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_category);
        context = this;

    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        subcategoryName = intent.getStringExtra("SUBCATEGORYNAME");
        subcategoryID = Integer.parseInt(subcategory = intent.getStringExtra("SUBCATEGORYID"));

        initializeUI();
        tv_productcategoryName.setText(subcategoryName);
        loadSubcategories();
    }

    private void initializeUI(){
        emptyIndicator = findViewById(R.id.view_Empty);
        tv_productcategoryCart = findViewById(R.id.tv_productcategoryCart);
        iv_productcategoryBack = findViewById(R.id.iv_productcategoryBack);
        iv_productcategoySearch = findViewById(R.id.iv_productcategoySearch);
        iv_productcategoryCart = findViewById(R.id.iv_productcategoryCart);
        iv_productcategoryMore = findViewById(R.id.iv_productcategoryMore);
        iv_productcategoryGrid = findViewById(R.id.iv_productcategoryGrid);
        iv_productcategoryNewest = findViewById(R.id.iv_productcategoryNewest);
        tv_productcategoryName = findViewById(R.id.tv_productcategoryName);
        iv_productcategoryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_productcategoryCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CartActivity.class);
                startActivity(intent);
            }
        });
        iv_productcategoryMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, MyAccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    private void loadSubcategories(){
        productcategoryRecyclerView = findViewById(R.id.productcategoryRecyclerView);
        sportsArrayList = new ArrayList<>();

        RequestParams params = new RequestParams();
        params.put("subcategory_id", subcategoryID);

        HttpProvider.post(context, "/mobile/subCategoryProduct", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Sports sportsModel = new Sports();
                            sportsModel.setId(id);
                            sportsModel.setName(name);
                            sportsModel.setAdded_by(added_by);
                            sportsModel.setCategory_id(category_id);
                            sportsModel.setSubcategory_id(subcategory_id);
                            sportsModel.setSubsubcategory_id(subsubcategory_id);
                            sportsModel.setBrand_id(brand_id);
                            sportsModel.setPhotos(photos);
                            sportsModel.setThumbnail_img(thumbnail_img);
                            sportsModel.setFeatured_img(featured_img);
                            sportsModel.setFlash_deal_img(flash_deal_img);
                            sportsModel.setVideo_provider(video_provider);
                            sportsModel.setVideo_link(video_link);
                            sportsModel.setTags(tags);
                            sportsModel.setDescription(description);
                            sportsModel.setUnit_price(unit_price);
                            sportsModel.setPurchase_price(purchase_price);
                            sportsModel.setChoice_options(choice_options);
                            sportsModel.setColors(colors);
                            sportsModel.setVariations(variations);
                            sportsModel.setTodays_deal(todays_deal);
                            sportsModel.setPublished(published);
                            sportsModel.setFeatured(featured);
                            sportsModel.setCurrent_stock(current_stock);
                            sportsModel.setUnit(unit);
                            sportsModel.setDiscount(discount);
                            sportsModel.setDiscount_type(discount_type);
                            sportsModel.setTax(tax);
                            sportsModel.setTax_type(tax_type);
                            sportsModel.setShipping_type(shipping_type);
                            sportsModel.setShipping_cost(shipping_cost);
                            sportsModel.setWeight(weight);
                            sportsModel.setParcel_size(parcel_size);
                            sportsModel.setNum_of_sale(num_of_sale);
                            sportsModel.setMeta_title(meta_title);
                            sportsModel.setMeta_description(meta_description);
                            sportsModel.setMeta_img(meta_img);
                            sportsModel.setPdf(pdf);
                            sportsModel.setSlug(slug);
                            sportsModel.setRating(rating);
                            sportsModel.setCreated_at(created_at);
                            sportsModel.setUpdated_at(updated_at);

                            sportsArrayList.add(sportsModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        productcategoryRecyclerView.setLayoutManager(layoutManager);

                        sportsProductAdapter = new SportsProductAdapter(context, sportsArrayList);
                        sportsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedSports = sportsArrayList.get(position);
                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMID", selectedSports.getId());
                                intent.putExtra("ITEMNAME", selectedSports.getName());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        productcategoryRecyclerView.setAdapter(sportsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

}
