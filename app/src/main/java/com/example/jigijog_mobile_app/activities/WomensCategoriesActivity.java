package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.AssordtedGridAdapter;
import com.example.jigijog_mobile_app.adapters.MensProductAdapter;
import com.example.jigijog_mobile_app.adapters.MensProductGridAdapter;
import com.example.jigijog_mobile_app.adapters.WomensProductAdapter;
import com.example.jigijog_mobile_app.adapters.WomensProductGridAdapter;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Assorted;
import com.example.jigijog_mobile_app.models.Men;
import com.example.jigijog_mobile_app.models.Women;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.Tools;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class WomensCategoriesActivity extends AppCompatActivity {

    private View view;
    private Context context;
    private String message;

    private ImageView iv_womenscategoryBack, iv_womenscategoryGrid,iv_womenscategoryMore, iv_womenscategoryCart;
    private TextView tv_womenscategoryName;

    private ConstraintLayout emptyIndicator;


    private RecyclerView womenscategoryRecyclerView;
    private ArrayList<Women> womenArrayList;
    private WomensProductAdapter womensProductAdapter;
    private WomensProductGridAdapter womensProductGridAdapter;

    private Women selectedWomen = new Women();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_womens_categories);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        message = intent.getStringExtra("SUBCATEGORY");
        initializeUI();

        if(message.equals("Top")) {
            tv_womenscategoryName.setText("Women's " + message);
            loadWomenCategoryTop();
            iv_womenscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadWomenCategoryTopGrid();
                }
            });
        }else if (message.equals("Bottom")){
            tv_womenscategoryName.setText("Women's " + message);
            loadWomenCategoryBottom();
            iv_womenscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadWomenCategoryBottomGrid();
                }
            });
        }else if (message.equals("Footwear")){
            tv_womenscategoryName.setText("Women's " + message);
            loadWomenCategoryFootwear();
            iv_womenscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadWomenCategoryFootwearGrid();
                }
            });
        }else if (message.equals("Sets & Accessories")){
            tv_womenscategoryName.setText("Women's " + message);
            loadWomenCategoryAccessories();
            iv_womenscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadWomenCategoryAccessoriesGrid();
                }
            });
        }
        else if (message.equals("Swimwear & Beachwear")){
            tv_womenscategoryName.setText("Women's " + message);
            //loadWomenCategorySwimwear();
            iv_womenscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //loadWomenCategorySwimwearGrid();
                }
            });
        }
    }

    private void initializeUI() {
        tv_womenscategoryName = findViewById(R.id.tv_womenscategoryName);
        iv_womenscategoryGrid = findViewById(R.id.iv_womenscategoryGrid);
        iv_womenscategoryBack = findViewById(R.id.iv_womenscategoryBack);
        iv_womenscategoryMore = findViewById(R.id.iv_womenscategoryMore);
        iv_womenscategoryCart = findViewById(R.id.iv_womenscategoryCart);
        iv_womenscategoryCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CartActivity.class);
                startActivity(intent);
            }
        });
        iv_womenscategoryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_womenscategoryMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, MyAccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    private void loadWomenCategoryTop() {
        womenscategoryRecyclerView = findViewById(R.id.womenscategoryRecyclerView);
        womenArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllWomensTop", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("Assorted: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Women womenModel = new Women();
                        womenModel.setId(id);
                        womenModel.setName(name);
                        womenModel.setAdded_by(added_by);
                        womenModel.setCategory_id(category_id);
                        womenModel.setSubcategory_id(subcategory_id);
                        womenModel.setSubsubcategory_id(subsubcategory_id);
                        womenModel.setBrand_id(brand_id);
                        womenModel.setPhotos(photos);
                        womenModel.setThumbnail_img(thumbnail_img);
                        womenModel.setFeatured_img(featured_img);
                        womenModel.setFlash_deal_img(flash_deal_img);
                        womenModel.setVideo_provider(video_provider);
                        womenModel.setVideo_link(video_link);
                        womenModel.setTags(tags);
                        womenModel.setDescription(description);
                        womenModel.setUnit_price(unit_price);
                        womenModel.setPurchase_price(purchase_price);
                        womenModel.setChoice_options(choice_options);
                        womenModel.setColors(colors);
                        womenModel.setVariations(variations);
                        womenModel.setTodays_deal(todays_deal);
                        womenModel.setPublished(published);
                        womenModel.setFeatured(featured);
                        womenModel.setCurrent_stock(current_stock);
                        womenModel.setUnit(unit);
                        womenModel.setDiscount(discount);
                        womenModel.setDiscount_type(discount_type);
                        womenModel.setTax(tax);
                        womenModel.setTax_type(tax_type);
                        womenModel.setShipping_type(shipping_type);
                        womenModel.setShipping_cost(shipping_cost);
                        womenModel.setWeight(weight);
                        womenModel.setParcel_size(parcel_size);
                        womenModel.setNum_of_sale(num_of_sale);
                        womenModel.setMeta_title(meta_title);
                        womenModel.setMeta_description(meta_description);
                        womenModel.setMeta_img(meta_img);
                        womenModel.setPdf(pdf);
                        womenModel.setSlug(slug);
                        womenModel.setRating(rating);
                        womenModel.setCreated_at(created_at);
                        womenModel.setUpdated_at(updated_at);

                        womenArrayList.add(womenModel);
                    }

                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                    womenscategoryRecyclerView.setLayoutManager(layoutManager);

                    womensProductAdapter = new WomensProductAdapter(context, womenArrayList);
                    womensProductAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedWomen = womenArrayList.get(position);

                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMNAME", selectedWomen.getName());
                            intent.putExtra("ITEMPRICE", selectedWomen.getUnit_price());
                            intent.putExtra("ITEMDISPRICE", selectedWomen.getDiscount());
                            intent.putExtra("ITEMIMAGE",selectedWomen.getFeatured_img());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    womenscategoryRecyclerView.setAdapter(womensProductAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }
    private void loadWomenCategoryBottom() {
        womenscategoryRecyclerView = findViewById(R.id.womenscategoryRecyclerView);
        womenArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllWomensBottom", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("Assorted: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Women womenModel = new Women();
                        womenModel.setId(id);
                        womenModel.setName(name);
                        womenModel.setAdded_by(added_by);
                        womenModel.setCategory_id(category_id);
                        womenModel.setSubcategory_id(subcategory_id);
                        womenModel.setSubsubcategory_id(subsubcategory_id);
                        womenModel.setBrand_id(brand_id);
                        womenModel.setPhotos(photos);
                        womenModel.setThumbnail_img(thumbnail_img);
                        womenModel.setFeatured_img(featured_img);
                        womenModel.setFlash_deal_img(flash_deal_img);
                        womenModel.setVideo_provider(video_provider);
                        womenModel.setVideo_link(video_link);
                        womenModel.setTags(tags);
                        womenModel.setDescription(description);
                        womenModel.setUnit_price(unit_price);
                        womenModel.setPurchase_price(purchase_price);
                        womenModel.setChoice_options(choice_options);
                        womenModel.setColors(colors);
                        womenModel.setVariations(variations);
                        womenModel.setTodays_deal(todays_deal);
                        womenModel.setPublished(published);
                        womenModel.setFeatured(featured);
                        womenModel.setCurrent_stock(current_stock);
                        womenModel.setUnit(unit);
                        womenModel.setDiscount(discount);
                        womenModel.setDiscount_type(discount_type);
                        womenModel.setTax(tax);
                        womenModel.setTax_type(tax_type);
                        womenModel.setShipping_type(shipping_type);
                        womenModel.setShipping_cost(shipping_cost);
                        womenModel.setWeight(weight);
                        womenModel.setParcel_size(parcel_size);
                        womenModel.setNum_of_sale(num_of_sale);
                        womenModel.setMeta_title(meta_title);
                        womenModel.setMeta_description(meta_description);
                        womenModel.setMeta_img(meta_img);
                        womenModel.setPdf(pdf);
                        womenModel.setSlug(slug);
                        womenModel.setRating(rating);
                        womenModel.setCreated_at(created_at);
                        womenModel.setUpdated_at(updated_at);

                        womenArrayList.add(womenModel);
                    }

                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                    womenscategoryRecyclerView.setLayoutManager(layoutManager);

                    womensProductAdapter = new WomensProductAdapter(context, womenArrayList);
                    womensProductAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedWomen = womenArrayList.get(position);

                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMNAME", selectedWomen.getName());
                            intent.putExtra("ITEMPRICE", selectedWomen.getUnit_price());
                            intent.putExtra("ITEMDISPRICE", selectedWomen.getDiscount());
                            intent.putExtra("ITEMIMAGE",selectedWomen.getFeatured_img());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    womenscategoryRecyclerView.setAdapter(womensProductAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadWomenCategoryFootwear() {
        womenscategoryRecyclerView = findViewById(R.id.womenscategoryRecyclerView);
        womenArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllWomensFootwear", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("Assorted: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Women womenModel = new Women();
                        womenModel.setId(id);
                        womenModel.setName(name);
                        womenModel.setAdded_by(added_by);
                        womenModel.setCategory_id(category_id);
                        womenModel.setSubcategory_id(subcategory_id);
                        womenModel.setSubsubcategory_id(subsubcategory_id);
                        womenModel.setBrand_id(brand_id);
                        womenModel.setPhotos(photos);
                        womenModel.setThumbnail_img(thumbnail_img);
                        womenModel.setFeatured_img(featured_img);
                        womenModel.setFlash_deal_img(flash_deal_img);
                        womenModel.setVideo_provider(video_provider);
                        womenModel.setVideo_link(video_link);
                        womenModel.setTags(tags);
                        womenModel.setDescription(description);
                        womenModel.setUnit_price(unit_price);
                        womenModel.setPurchase_price(purchase_price);
                        womenModel.setChoice_options(choice_options);
                        womenModel.setColors(colors);
                        womenModel.setVariations(variations);
                        womenModel.setTodays_deal(todays_deal);
                        womenModel.setPublished(published);
                        womenModel.setFeatured(featured);
                        womenModel.setCurrent_stock(current_stock);
                        womenModel.setUnit(unit);
                        womenModel.setDiscount(discount);
                        womenModel.setDiscount_type(discount_type);
                        womenModel.setTax(tax);
                        womenModel.setTax_type(tax_type);
                        womenModel.setShipping_type(shipping_type);
                        womenModel.setShipping_cost(shipping_cost);
                        womenModel.setWeight(weight);
                        womenModel.setParcel_size(parcel_size);
                        womenModel.setNum_of_sale(num_of_sale);
                        womenModel.setMeta_title(meta_title);
                        womenModel.setMeta_description(meta_description);
                        womenModel.setMeta_img(meta_img);
                        womenModel.setPdf(pdf);
                        womenModel.setSlug(slug);
                        womenModel.setRating(rating);
                        womenModel.setCreated_at(created_at);
                        womenModel.setUpdated_at(updated_at);

                        womenArrayList.add(womenModel);
                    }

                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                    womenscategoryRecyclerView.setLayoutManager(layoutManager);

                    womensProductAdapter = new WomensProductAdapter(context, womenArrayList);
                    womensProductAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedWomen = womenArrayList.get(position);

                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMNAME", selectedWomen.getName());
                            intent.putExtra("ITEMPRICE", selectedWomen.getUnit_price());
                            intent.putExtra("ITEMDISPRICE", selectedWomen.getDiscount());
                            intent.putExtra("ITEMIMAGE",selectedWomen.getFeatured_img());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    womenscategoryRecyclerView.setAdapter(womensProductAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadWomenCategoryAccessories() {
        womenscategoryRecyclerView = findViewById(R.id.womenscategoryRecyclerView);
        womenArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllWomensSetsAccessories", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("Assorted: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Women womenModel = new Women();
                        womenModel.setId(id);
                        womenModel.setName(name);
                        womenModel.setAdded_by(added_by);
                        womenModel.setCategory_id(category_id);
                        womenModel.setSubcategory_id(subcategory_id);
                        womenModel.setSubsubcategory_id(subsubcategory_id);
                        womenModel.setBrand_id(brand_id);
                        womenModel.setPhotos(photos);
                        womenModel.setThumbnail_img(thumbnail_img);
                        womenModel.setFeatured_img(featured_img);
                        womenModel.setFlash_deal_img(flash_deal_img);
                        womenModel.setVideo_provider(video_provider);
                        womenModel.setVideo_link(video_link);
                        womenModel.setTags(tags);
                        womenModel.setDescription(description);
                        womenModel.setUnit_price(unit_price);
                        womenModel.setPurchase_price(purchase_price);
                        womenModel.setChoice_options(choice_options);
                        womenModel.setColors(colors);
                        womenModel.setVariations(variations);
                        womenModel.setTodays_deal(todays_deal);
                        womenModel.setPublished(published);
                        womenModel.setFeatured(featured);
                        womenModel.setCurrent_stock(current_stock);
                        womenModel.setUnit(unit);
                        womenModel.setDiscount(discount);
                        womenModel.setDiscount_type(discount_type);
                        womenModel.setTax(tax);
                        womenModel.setTax_type(tax_type);
                        womenModel.setShipping_type(shipping_type);
                        womenModel.setShipping_cost(shipping_cost);
                        womenModel.setWeight(weight);
                        womenModel.setParcel_size(parcel_size);
                        womenModel.setNum_of_sale(num_of_sale);
                        womenModel.setMeta_title(meta_title);
                        womenModel.setMeta_description(meta_description);
                        womenModel.setMeta_img(meta_img);
                        womenModel.setPdf(pdf);
                        womenModel.setSlug(slug);
                        womenModel.setRating(rating);
                        womenModel.setCreated_at(created_at);
                        womenModel.setUpdated_at(updated_at);

                        womenArrayList.add(womenModel);
                    }

                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                    womenscategoryRecyclerView.setLayoutManager(layoutManager);

                    womensProductAdapter = new WomensProductAdapter(context, womenArrayList);
                    womensProductAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedWomen = womenArrayList.get(position);

                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMNAME", selectedWomen.getName());
                            intent.putExtra("ITEMPRICE", selectedWomen.getUnit_price());
                            intent.putExtra("ITEMDISPRICE", selectedWomen.getDiscount());
                            intent.putExtra("ITEMIMAGE",selectedWomen.getFeatured_img());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    womenscategoryRecyclerView.setAdapter(womensProductAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
//    private void loadWomenCategorySwimwear() {
//        womenscategoryRecyclerView = findViewById(R.id.womenscategoryRecyclerView);
//        womenArrayList = new ArrayList<>();
//
//        womenArrayList.add(new Women("900.00", "1500.00", "Womens Casual Swimwear 1",R.drawable.womenswim1));
//        womenArrayList.add(new Women("850.00", "", "Womens Casual Swimwear 2",R.drawable.womenswim2));
//        womenArrayList.add(new Women("550.00", "990.00", "Womens Casual Swimwear 3",R.drawable.womenswim3));
//        womenArrayList.add(new Women("930.00", "1200.00", "Womens Casual Swimwear 4",R.drawable.womenswim4));
//        womenArrayList.add(new Women("150.00", "200.00", "Womens Casual Swimwear 5",R.drawable.womenswim5));
//
//        womenArrayList.add(new Women("900.00", "1500.00", "Womens Casual Swimwear 1",R.drawable.womenswim1));
//        womenArrayList.add(new Women("850.00", "", "Womens Casual Swimwear 2",R.drawable.womenswim2));
//        womenArrayList.add(new Women("550.00", "990.00", "Womens Casual Swimwear 3",R.drawable.womenswim3));
//        womenArrayList.add(new Women("930.00", "1200.00", "Womens Casual Swimwear 4",R.drawable.womenswim4));
//        womenArrayList.add(new Women("150.00", "200.00", "Womens Casual Swimwear 5",R.drawable.womenswim5));
//
//        womenArrayList.add(new Women("900.00", "1500.00", "Womens Casual Swimwear 1",R.drawable.womenswim1));
//        womenArrayList.add(new Women("850.00", "", "Womens Casual Swimwear 2",R.drawable.womenswim2));
//        womenArrayList.add(new Women("550.00", "990.00", "Womens Casual Swimwear 3",R.drawable.womenswim3));
//        womenArrayList.add(new Women("930.00", "1200.00", "Womens Casual Swimwear 4",R.drawable.womenswim4));
//        womenArrayList.add(new Women("150.00", "200.00", "Womens Casual Swimwear 5",R.drawable.womenswim5));
//
//        womenArrayList.add(new Women("900.00", "1500.00", "Womens Casual Swimwear 1",R.drawable.womenswim1));
//        womenArrayList.add(new Women("850.00", "", "Womens Casual Swimwear 2",R.drawable.womenswim2));
//        womenArrayList.add(new Women("550.00", "990.00", "Womens Casual Swimwear 3",R.drawable.womenswim3));
//        womenArrayList.add(new Women("930.00", "1200.00", "Womens Casual Swimwear 4",R.drawable.womenswim4));
//        womenArrayList.add(new Women("150.00", "200.00", "Womens Casual Swimwear 5",R.drawable.womenswim5));
//
//        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
//        womenscategoryRecyclerView.setLayoutManager(layoutManager);
//
//        womensProductAdapter = new WomensProductAdapter(context, womenArrayList);
//        womensProductAdapter.setClickListener(new OnClickRecyclerView() {
//            @Override
//            public void onItemClick(View view, int position) {
//                selectedWomen = womenArrayList.get(position);
//
//                Bundle extras = new Bundle();
//                Intent intent = new Intent(context, ProductViewActivity.class);
//                intent.putExtra("ITEMNAME", selectedWomen.getWomenItemName());
//                intent.putExtra("ITEMPRICE", selectedWomen.getWomenPrice());
//                intent.putExtra("ITEMDISPRICE", selectedWomen.getWomenDisPrice());
//                intent.putExtra("ITEMIMAGE",selectedWomen.getWomenImage());
//                intent.putExtras(extras);
//                startActivity(intent);
//            }
//        });
//        womenscategoryRecyclerView.setAdapter(womensProductAdapter);
//    }

    private void loadWomenCategoryTopGrid() {
        womenscategoryRecyclerView = findViewById(R.id.womenscategoryRecyclerView);
        womenArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllWomensTop", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("Assorted: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Women womenModel = new Women();
                        womenModel.setId(id);
                        womenModel.setName(name);
                        womenModel.setAdded_by(added_by);
                        womenModel.setCategory_id(category_id);
                        womenModel.setSubcategory_id(subcategory_id);
                        womenModel.setSubsubcategory_id(subsubcategory_id);
                        womenModel.setBrand_id(brand_id);
                        womenModel.setPhotos(photos);
                        womenModel.setThumbnail_img(thumbnail_img);
                        womenModel.setFeatured_img(featured_img);
                        womenModel.setFlash_deal_img(flash_deal_img);
                        womenModel.setVideo_provider(video_provider);
                        womenModel.setVideo_link(video_link);
                        womenModel.setTags(tags);
                        womenModel.setDescription(description);
                        womenModel.setUnit_price(unit_price);
                        womenModel.setPurchase_price(purchase_price);
                        womenModel.setChoice_options(choice_options);
                        womenModel.setColors(colors);
                        womenModel.setVariations(variations);
                        womenModel.setTodays_deal(todays_deal);
                        womenModel.setPublished(published);
                        womenModel.setFeatured(featured);
                        womenModel.setCurrent_stock(current_stock);
                        womenModel.setUnit(unit);
                        womenModel.setDiscount(discount);
                        womenModel.setDiscount_type(discount_type);
                        womenModel.setTax(tax);
                        womenModel.setTax_type(tax_type);
                        womenModel.setShipping_type(shipping_type);
                        womenModel.setShipping_cost(shipping_cost);
                        womenModel.setWeight(weight);
                        womenModel.setParcel_size(parcel_size);
                        womenModel.setNum_of_sale(num_of_sale);
                        womenModel.setMeta_title(meta_title);
                        womenModel.setMeta_description(meta_description);
                        womenModel.setMeta_img(meta_img);
                        womenModel.setPdf(pdf);
                        womenModel.setSlug(slug);
                        womenModel.setRating(rating);
                        womenModel.setCreated_at(created_at);
                        womenModel.setUpdated_at(updated_at);

                        womenArrayList.add(womenModel);
                    }


                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                    womenscategoryRecyclerView.setLayoutManager(layoutManager);

                    womensProductGridAdapter = new WomensProductGridAdapter(context, womenArrayList);
                    womensProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedWomen = womenArrayList.get(position);

                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMNAME", selectedWomen.getName());
                            intent.putExtra("ITEMPRICE", selectedWomen.getUnit_price());
                            intent.putExtra("ITEMDISPRICE", selectedWomen.getDiscount());
                            intent.putExtra("ITEMIMAGE",selectedWomen.getFeatured_img());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    womenscategoryRecyclerView.setAdapter(womensProductGridAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    private void loadWomenCategoryBottomGrid() {
        womenscategoryRecyclerView = findViewById(R.id.womenscategoryRecyclerView);
        womenArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllWomensBottom", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("Assorted: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Women womenModel = new Women();
                        womenModel.setId(id);
                        womenModel.setName(name);
                        womenModel.setAdded_by(added_by);
                        womenModel.setCategory_id(category_id);
                        womenModel.setSubcategory_id(subcategory_id);
                        womenModel.setSubsubcategory_id(subsubcategory_id);
                        womenModel.setBrand_id(brand_id);
                        womenModel.setPhotos(photos);
                        womenModel.setThumbnail_img(thumbnail_img);
                        womenModel.setFeatured_img(featured_img);
                        womenModel.setFlash_deal_img(flash_deal_img);
                        womenModel.setVideo_provider(video_provider);
                        womenModel.setVideo_link(video_link);
                        womenModel.setTags(tags);
                        womenModel.setDescription(description);
                        womenModel.setUnit_price(unit_price);
                        womenModel.setPurchase_price(purchase_price);
                        womenModel.setChoice_options(choice_options);
                        womenModel.setColors(colors);
                        womenModel.setVariations(variations);
                        womenModel.setTodays_deal(todays_deal);
                        womenModel.setPublished(published);
                        womenModel.setFeatured(featured);
                        womenModel.setCurrent_stock(current_stock);
                        womenModel.setUnit(unit);
                        womenModel.setDiscount(discount);
                        womenModel.setDiscount_type(discount_type);
                        womenModel.setTax(tax);
                        womenModel.setTax_type(tax_type);
                        womenModel.setShipping_type(shipping_type);
                        womenModel.setShipping_cost(shipping_cost);
                        womenModel.setWeight(weight);
                        womenModel.setParcel_size(parcel_size);
                        womenModel.setNum_of_sale(num_of_sale);
                        womenModel.setMeta_title(meta_title);
                        womenModel.setMeta_description(meta_description);
                        womenModel.setMeta_img(meta_img);
                        womenModel.setPdf(pdf);
                        womenModel.setSlug(slug);
                        womenModel.setRating(rating);
                        womenModel.setCreated_at(created_at);
                        womenModel.setUpdated_at(updated_at);

                        womenArrayList.add(womenModel);
                    }


                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                    womenscategoryRecyclerView.setLayoutManager(layoutManager);

                    womensProductGridAdapter = new WomensProductGridAdapter(context, womenArrayList);
                    womensProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedWomen = womenArrayList.get(position);

                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMNAME", selectedWomen.getName());
                            intent.putExtra("ITEMPRICE", selectedWomen.getUnit_price());
                            intent.putExtra("ITEMDISPRICE", selectedWomen.getDiscount());
                            intent.putExtra("ITEMIMAGE",selectedWomen.getFeatured_img());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    womenscategoryRecyclerView.setAdapter(womensProductGridAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }
    private void loadWomenCategoryFootwearGrid() {
        womenscategoryRecyclerView = findViewById(R.id.womenscategoryRecyclerView);
        womenArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllWomensFootwear", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("Assorted: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Women womenModel = new Women();
                        womenModel.setId(id);
                        womenModel.setName(name);
                        womenModel.setAdded_by(added_by);
                        womenModel.setCategory_id(category_id);
                        womenModel.setSubcategory_id(subcategory_id);
                        womenModel.setSubsubcategory_id(subsubcategory_id);
                        womenModel.setBrand_id(brand_id);
                        womenModel.setPhotos(photos);
                        womenModel.setThumbnail_img(thumbnail_img);
                        womenModel.setFeatured_img(featured_img);
                        womenModel.setFlash_deal_img(flash_deal_img);
                        womenModel.setVideo_provider(video_provider);
                        womenModel.setVideo_link(video_link);
                        womenModel.setTags(tags);
                        womenModel.setDescription(description);
                        womenModel.setUnit_price(unit_price);
                        womenModel.setPurchase_price(purchase_price);
                        womenModel.setChoice_options(choice_options);
                        womenModel.setColors(colors);
                        womenModel.setVariations(variations);
                        womenModel.setTodays_deal(todays_deal);
                        womenModel.setPublished(published);
                        womenModel.setFeatured(featured);
                        womenModel.setCurrent_stock(current_stock);
                        womenModel.setUnit(unit);
                        womenModel.setDiscount(discount);
                        womenModel.setDiscount_type(discount_type);
                        womenModel.setTax(tax);
                        womenModel.setTax_type(tax_type);
                        womenModel.setShipping_type(shipping_type);
                        womenModel.setShipping_cost(shipping_cost);
                        womenModel.setWeight(weight);
                        womenModel.setParcel_size(parcel_size);
                        womenModel.setNum_of_sale(num_of_sale);
                        womenModel.setMeta_title(meta_title);
                        womenModel.setMeta_description(meta_description);
                        womenModel.setMeta_img(meta_img);
                        womenModel.setPdf(pdf);
                        womenModel.setSlug(slug);
                        womenModel.setRating(rating);
                        womenModel.setCreated_at(created_at);
                        womenModel.setUpdated_at(updated_at);

                        womenArrayList.add(womenModel);
                    }


                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                    womenscategoryRecyclerView.setLayoutManager(layoutManager);

                    womensProductGridAdapter = new WomensProductGridAdapter(context, womenArrayList);
                    womensProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedWomen = womenArrayList.get(position);

                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMNAME", selectedWomen.getName());
                            intent.putExtra("ITEMPRICE", selectedWomen.getUnit_price());
                            intent.putExtra("ITEMDISPRICE", selectedWomen.getDiscount());
                            intent.putExtra("ITEMIMAGE",selectedWomen.getFeatured_img());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    womenscategoryRecyclerView.setAdapter(womensProductGridAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadWomenCategoryAccessoriesGrid() {
        womenscategoryRecyclerView = findViewById(R.id.womenscategoryRecyclerView);
        womenArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllWomensSetsAccessories", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                Debugger.logD("Assorted: ");
                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");
                        //ArrayList<String> shit = jsonObject.getString("shit");

                        Women womenModel = new Women();
                        womenModel.setId(id);
                        womenModel.setName(name);
                        womenModel.setAdded_by(added_by);
                        womenModel.setCategory_id(category_id);
                        womenModel.setSubcategory_id(subcategory_id);
                        womenModel.setSubsubcategory_id(subsubcategory_id);
                        womenModel.setBrand_id(brand_id);
                        womenModel.setPhotos(photos);
                        womenModel.setThumbnail_img(thumbnail_img);
                        womenModel.setFeatured_img(featured_img);
                        womenModel.setFlash_deal_img(flash_deal_img);
                        womenModel.setVideo_provider(video_provider);
                        womenModel.setVideo_link(video_link);
                        womenModel.setTags(tags);
                        womenModel.setDescription(description);
                        womenModel.setUnit_price(unit_price);
                        womenModel.setPurchase_price(purchase_price);
                        womenModel.setChoice_options(choice_options);
                        womenModel.setColors(colors);
                        womenModel.setVariations(variations);
                        womenModel.setTodays_deal(todays_deal);
                        womenModel.setPublished(published);
                        womenModel.setFeatured(featured);
                        womenModel.setCurrent_stock(current_stock);
                        womenModel.setUnit(unit);
                        womenModel.setDiscount(discount);
                        womenModel.setDiscount_type(discount_type);
                        womenModel.setTax(tax);
                        womenModel.setTax_type(tax_type);
                        womenModel.setShipping_type(shipping_type);
                        womenModel.setShipping_cost(shipping_cost);
                        womenModel.setWeight(weight);
                        womenModel.setParcel_size(parcel_size);
                        womenModel.setNum_of_sale(num_of_sale);
                        womenModel.setMeta_title(meta_title);
                        womenModel.setMeta_description(meta_description);
                        womenModel.setMeta_img(meta_img);
                        womenModel.setPdf(pdf);
                        womenModel.setSlug(slug);
                        womenModel.setRating(rating);
                        womenModel.setCreated_at(created_at);
                        womenModel.setUpdated_at(updated_at);

                        womenArrayList.add(womenModel);
                    }


                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                    womenscategoryRecyclerView.setLayoutManager(layoutManager);

                    womensProductGridAdapter = new WomensProductGridAdapter(context, womenArrayList);
                    womensProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedWomen = womenArrayList.get(position);

                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMNAME", selectedWomen.getName());
                            intent.putExtra("ITEMPRICE", selectedWomen.getUnit_price());
                            intent.putExtra("ITEMDISPRICE", selectedWomen.getDiscount());
                            intent.putExtra("ITEMIMAGE",selectedWomen.getFeatured_img());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    womenscategoryRecyclerView.setAdapter(womensProductGridAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
//    private void loadWomenCategorySwimwearGrid() {
//        womenscategoryRecyclerView = findViewById(R.id.womenscategoryRecyclerView);
//        womenArrayList = new ArrayList<>();
//
//        womenArrayList.add(new Women("900.00", "1500.00", "Womens Casual Swimwear 1",R.drawable.womenswim1));
//        womenArrayList.add(new Women("850.00", "", "Womens Casual Swimwear 2",R.drawable.womenswim2));
//        womenArrayList.add(new Women("550.00", "990.00", "Womens Casual Swimwear 3",R.drawable.womenswim3));
//        womenArrayList.add(new Women("930.00", "1200.00", "Womens Casual Swimwear 4",R.drawable.womenswim4));
//        womenArrayList.add(new Women("150.00", "200.00", "Womens Casual Swimwear 5",R.drawable.womenswim5));
//
//        womenArrayList.add(new Women("900.00", "1500.00", "Womens Casual Swimwear 1",R.drawable.womenswim1));
//        womenArrayList.add(new Women("850.00", "", "Womens Casual Swimwear 2",R.drawable.womenswim2));
//        womenArrayList.add(new Women("550.00", "990.00", "Womens Casual Swimwear 3",R.drawable.womenswim3));
//        womenArrayList.add(new Women("930.00", "1200.00", "Womens Casual Swimwear 4",R.drawable.womenswim4));
//        womenArrayList.add(new Women("150.00", "200.00", "Womens Casual Swimwear 5",R.drawable.womenswim5));
//
//        womenArrayList.add(new Women("900.00", "1500.00", "Womens Casual Swimwear 1",R.drawable.womenswim1));
//        womenArrayList.add(new Women("850.00", "", "Womens Casual Swimwear 2",R.drawable.womenswim2));
//        womenArrayList.add(new Women("550.00", "990.00", "Womens Casual Swimwear 3",R.drawable.womenswim3));
//        womenArrayList.add(new Women("930.00", "1200.00", "Womens Casual Swimwear 4",R.drawable.womenswim4));
//        womenArrayList.add(new Women("150.00", "200.00", "Womens Casual Swimwear 5",R.drawable.womenswim5));
//
//        womenArrayList.add(new Women("900.00", "1500.00", "Womens Casual Swimwear 1",R.drawable.womenswim1));
//        womenArrayList.add(new Women("850.00", "", "Womens Casual Swimwear 2",R.drawable.womenswim2));
//        womenArrayList.add(new Women("550.00", "990.00", "Womens Casual Swimwear 3",R.drawable.womenswim3));
//        womenArrayList.add(new Women("930.00", "1200.00", "Womens Casual Swimwear 4",R.drawable.womenswim4));
//        womenArrayList.add(new Women("150.00", "200.00", "Womens Casual Swimwear 5",R.drawable.womenswim5));
//
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
//        womenscategoryRecyclerView.setLayoutManager(layoutManager);
//
//        womensProductGridAdapter = new WomensProductGridAdapter(context, womenArrayList);
//        womensProductGridAdapter.setClickListener(new OnClickRecyclerView() {
//            @Override
//            public void onItemClick(View view, int position) {
//                selectedWomen = womenArrayList.get(position);
//
//                Bundle extras = new Bundle();
//                Intent intent = new Intent(context, ProductViewActivity.class);
//                intent.putExtra("ITEMNAME", selectedWomen.getWomenItemName());
//                intent.putExtra("ITEMPRICE", selectedWomen.getWomenPrice());
//                intent.putExtra("ITEMDISPRICE", selectedWomen.getWomenDisPrice());
//                intent.putExtra("ITEMIMAGE",selectedWomen.getWomenImage());
//                intent.putExtras(extras);
//                startActivity(intent);
//            }
//        });
//        womenscategoryRecyclerView.setAdapter(womensProductGridAdapter);
//    }

}
