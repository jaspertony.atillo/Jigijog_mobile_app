package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.JewelryProductAdapter;
import com.example.jigijog_mobile_app.adapters.JewelryProductGridAdapter;
import com.example.jigijog_mobile_app.adapters.SportsProductAdapter;
import com.example.jigijog_mobile_app.adapters.WomensProductAdapter;
import com.example.jigijog_mobile_app.adapters.WomensProductGridAdapter;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Jewelry;
import com.example.jigijog_mobile_app.models.Sports;
import com.example.jigijog_mobile_app.models.Women;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.Tools;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class JewelryCategoriesActivity extends AppCompatActivity {

    private View view;
    private Context context;
    private String message;

    private ImageView iv_jewelrycategoryBack, iv_jewelrycategoryGrid,iv_jewelrycategoryMore, iv_jewelrycategoryCart;
    private TextView tv_jewelrycategoryName;

    private ConstraintLayout emptyIndicator;


    private RecyclerView jewelrycategoryRecyclerView;
    private ArrayList<Jewelry> jewelryArrayList;
    private JewelryProductAdapter jewelryProductAdapter;
    private JewelryProductGridAdapter jewelryProductGridAdapter;

    private Jewelry selectedJewelry = new Jewelry();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jewelry_categories);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        message = intent.getStringExtra("SUBCATEGORY");
        initializeUI();
        if(message.equals("Fashion Watches")) {
            tv_jewelrycategoryName.setText( message);
            loadJewelryFashion();
            iv_jewelrycategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadJewelryFashionGrid();
                }
            });
        }else if (message.equals("Jewelry")){
            tv_jewelrycategoryName.setText( message);
            loadJewelryJewelries();
            iv_jewelrycategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadJewelryJewelriesGrid();
                }
            });
        }
    }

    private void initializeUI() {
        emptyIndicator = findViewById(R.id.view_Empty);
        tv_jewelrycategoryName = findViewById(R.id.tv_jewelrycategoryName);
        iv_jewelrycategoryGrid = findViewById(R.id.iv_jewelrycategoryGrid);
        iv_jewelrycategoryBack = findViewById(R.id.iv_jewelrycategoryBack);
        iv_jewelrycategoryCart = findViewById(R.id.iv_jewelrycategoryCart);
        iv_jewelrycategoryCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CartActivity.class);
                startActivity(intent);
            }
        });
        iv_jewelrycategoryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_jewelrycategoryMore = findViewById(R.id.iv_jewelrycategoryMore);
        iv_jewelrycategoryMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, MyAccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }
    private void loadJewelryFashion(){
        jewelrycategoryRecyclerView = findViewById(R.id.jewelrycategoryRecyclerView);
        jewelryArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllFashionWatches", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Jewelry jewelryModel = new Jewelry();
                            jewelryModel.setId(id);
                            jewelryModel.setName(name);
                            jewelryModel.setAdded_by(added_by);
                            jewelryModel.setCategory_id(category_id);
                            jewelryModel.setSubcategory_id(subcategory_id);
                            jewelryModel.setSubsubcategory_id(subsubcategory_id);
                            jewelryModel.setBrand_id(brand_id);
                            jewelryModel.setPhotos(photos);
                            jewelryModel.setThumbnail_img(thumbnail_img);
                            jewelryModel.setFeatured_img(featured_img);
                            jewelryModel.setFlash_deal_img(flash_deal_img);
                            jewelryModel.setVideo_provider(video_provider);
                            jewelryModel.setVideo_link(video_link);
                            jewelryModel.setTags(tags);
                            jewelryModel.setDescription(description);
                            jewelryModel.setUnit_price(unit_price);
                            jewelryModel.setPurchase_price(purchase_price);
                            jewelryModel.setChoice_options(choice_options);
                            jewelryModel.setColors(colors);
                            jewelryModel.setVariations(variations);
                            jewelryModel.setTodays_deal(todays_deal);
                            jewelryModel.setPublished(published);
                            jewelryModel.setFeatured(featured);
                            jewelryModel.setCurrent_stock(current_stock);
                            jewelryModel.setUnit(unit);
                            jewelryModel.setDiscount(discount);
                            jewelryModel.setDiscount_type(discount_type);
                            jewelryModel.setTax(tax);
                            jewelryModel.setTax_type(tax_type);
                            jewelryModel.setShipping_type(shipping_type);
                            jewelryModel.setShipping_cost(shipping_cost);
                            jewelryModel.setWeight(weight);
                            jewelryModel.setParcel_size(parcel_size);
                            jewelryModel.setNum_of_sale(num_of_sale);
                            jewelryModel.setMeta_title(meta_title);
                            jewelryModel.setMeta_description(meta_description);
                            jewelryModel.setMeta_img(meta_img);
                            jewelryModel.setPdf(pdf);
                            jewelryModel.setSlug(slug);
                            jewelryModel.setRating(rating);
                            jewelryModel.setCreated_at(created_at);
                            jewelryModel.setUpdated_at(updated_at);

                            jewelryArrayList.add(jewelryModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        jewelrycategoryRecyclerView.setLayoutManager(layoutManager);

                        jewelryProductAdapter = new JewelryProductAdapter(context, jewelryArrayList);
                        jewelryProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedJewelry = jewelryArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedJewelry.getName());
                                intent.putExtra("ITEMPRICE", selectedJewelry.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedJewelry.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedJewelry.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedJewelry.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        jewelrycategoryRecyclerView.setAdapter(jewelryProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadJewelryJewelries(){
        jewelrycategoryRecyclerView = findViewById(R.id.jewelrycategoryRecyclerView);
        jewelryArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllJewelry", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Jewelry jewelryModel = new Jewelry();
                            jewelryModel.setId(id);
                            jewelryModel.setName(name);
                            jewelryModel.setAdded_by(added_by);
                            jewelryModel.setCategory_id(category_id);
                            jewelryModel.setSubcategory_id(subcategory_id);
                            jewelryModel.setSubsubcategory_id(subsubcategory_id);
                            jewelryModel.setBrand_id(brand_id);
                            jewelryModel.setPhotos(photos);
                            jewelryModel.setThumbnail_img(thumbnail_img);
                            jewelryModel.setFeatured_img(featured_img);
                            jewelryModel.setFlash_deal_img(flash_deal_img);
                            jewelryModel.setVideo_provider(video_provider);
                            jewelryModel.setVideo_link(video_link);
                            jewelryModel.setTags(tags);
                            jewelryModel.setDescription(description);
                            jewelryModel.setUnit_price(unit_price);
                            jewelryModel.setPurchase_price(purchase_price);
                            jewelryModel.setChoice_options(choice_options);
                            jewelryModel.setColors(colors);
                            jewelryModel.setVariations(variations);
                            jewelryModel.setTodays_deal(todays_deal);
                            jewelryModel.setPublished(published);
                            jewelryModel.setFeatured(featured);
                            jewelryModel.setCurrent_stock(current_stock);
                            jewelryModel.setUnit(unit);
                            jewelryModel.setDiscount(discount);
                            jewelryModel.setDiscount_type(discount_type);
                            jewelryModel.setTax(tax);
                            jewelryModel.setTax_type(tax_type);
                            jewelryModel.setShipping_type(shipping_type);
                            jewelryModel.setShipping_cost(shipping_cost);
                            jewelryModel.setWeight(weight);
                            jewelryModel.setParcel_size(parcel_size);
                            jewelryModel.setNum_of_sale(num_of_sale);
                            jewelryModel.setMeta_title(meta_title);
                            jewelryModel.setMeta_description(meta_description);
                            jewelryModel.setMeta_img(meta_img);
                            jewelryModel.setPdf(pdf);
                            jewelryModel.setSlug(slug);
                            jewelryModel.setRating(rating);
                            jewelryModel.setCreated_at(created_at);
                            jewelryModel.setUpdated_at(updated_at);

                            jewelryArrayList.add(jewelryModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        jewelrycategoryRecyclerView.setLayoutManager(layoutManager);

                        jewelryProductAdapter = new JewelryProductAdapter(context, jewelryArrayList);
                        jewelryProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedJewelry = jewelryArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedJewelry.getName());
                                intent.putExtra("ITEMPRICE", selectedJewelry.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedJewelry.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedJewelry.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedJewelry.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        jewelrycategoryRecyclerView.setAdapter(jewelryProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadJewelryFashionGrid(){
        jewelrycategoryRecyclerView = findViewById(R.id.jewelrycategoryRecyclerView);
        jewelryArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllFashionWatches", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Jewelry jewelryModel = new Jewelry();
                            jewelryModel.setId(id);
                            jewelryModel.setName(name);
                            jewelryModel.setAdded_by(added_by);
                            jewelryModel.setCategory_id(category_id);
                            jewelryModel.setSubcategory_id(subcategory_id);
                            jewelryModel.setSubsubcategory_id(subsubcategory_id);
                            jewelryModel.setBrand_id(brand_id);
                            jewelryModel.setPhotos(photos);
                            jewelryModel.setThumbnail_img(thumbnail_img);
                            jewelryModel.setFeatured_img(featured_img);
                            jewelryModel.setFlash_deal_img(flash_deal_img);
                            jewelryModel.setVideo_provider(video_provider);
                            jewelryModel.setVideo_link(video_link);
                            jewelryModel.setTags(tags);
                            jewelryModel.setDescription(description);
                            jewelryModel.setUnit_price(unit_price);
                            jewelryModel.setPurchase_price(purchase_price);
                            jewelryModel.setChoice_options(choice_options);
                            jewelryModel.setColors(colors);
                            jewelryModel.setVariations(variations);
                            jewelryModel.setTodays_deal(todays_deal);
                            jewelryModel.setPublished(published);
                            jewelryModel.setFeatured(featured);
                            jewelryModel.setCurrent_stock(current_stock);
                            jewelryModel.setUnit(unit);
                            jewelryModel.setDiscount(discount);
                            jewelryModel.setDiscount_type(discount_type);
                            jewelryModel.setTax(tax);
                            jewelryModel.setTax_type(tax_type);
                            jewelryModel.setShipping_type(shipping_type);
                            jewelryModel.setShipping_cost(shipping_cost);
                            jewelryModel.setWeight(weight);
                            jewelryModel.setParcel_size(parcel_size);
                            jewelryModel.setNum_of_sale(num_of_sale);
                            jewelryModel.setMeta_title(meta_title);
                            jewelryModel.setMeta_description(meta_description);
                            jewelryModel.setMeta_img(meta_img);
                            jewelryModel.setPdf(pdf);
                            jewelryModel.setSlug(slug);
                            jewelryModel.setRating(rating);
                            jewelryModel.setCreated_at(created_at);
                            jewelryModel.setUpdated_at(updated_at);

                            jewelryArrayList.add(jewelryModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        jewelrycategoryRecyclerView.setLayoutManager(layoutManager);

                        jewelryProductGridAdapter = new JewelryProductGridAdapter(context, jewelryArrayList);
                        jewelryProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedJewelry = jewelryArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedJewelry.getName());
                                intent.putExtra("ITEMPRICE", selectedJewelry.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedJewelry.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedJewelry.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedJewelry.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        jewelrycategoryRecyclerView.setAdapter(jewelryProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadJewelryJewelriesGrid(){
        jewelrycategoryRecyclerView = findViewById(R.id.jewelrycategoryRecyclerView);
        jewelryArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllJewelry", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Jewelry jewelryModel = new Jewelry();
                            jewelryModel.setId(id);
                            jewelryModel.setName(name);
                            jewelryModel.setAdded_by(added_by);
                            jewelryModel.setCategory_id(category_id);
                            jewelryModel.setSubcategory_id(subcategory_id);
                            jewelryModel.setSubsubcategory_id(subsubcategory_id);
                            jewelryModel.setBrand_id(brand_id);
                            jewelryModel.setPhotos(photos);
                            jewelryModel.setThumbnail_img(thumbnail_img);
                            jewelryModel.setFeatured_img(featured_img);
                            jewelryModel.setFlash_deal_img(flash_deal_img);
                            jewelryModel.setVideo_provider(video_provider);
                            jewelryModel.setVideo_link(video_link);
                            jewelryModel.setTags(tags);
                            jewelryModel.setDescription(description);
                            jewelryModel.setUnit_price(unit_price);
                            jewelryModel.setPurchase_price(purchase_price);
                            jewelryModel.setChoice_options(choice_options);
                            jewelryModel.setColors(colors);
                            jewelryModel.setVariations(variations);
                            jewelryModel.setTodays_deal(todays_deal);
                            jewelryModel.setPublished(published);
                            jewelryModel.setFeatured(featured);
                            jewelryModel.setCurrent_stock(current_stock);
                            jewelryModel.setUnit(unit);
                            jewelryModel.setDiscount(discount);
                            jewelryModel.setDiscount_type(discount_type);
                            jewelryModel.setTax(tax);
                            jewelryModel.setTax_type(tax_type);
                            jewelryModel.setShipping_type(shipping_type);
                            jewelryModel.setShipping_cost(shipping_cost);
                            jewelryModel.setWeight(weight);
                            jewelryModel.setParcel_size(parcel_size);
                            jewelryModel.setNum_of_sale(num_of_sale);
                            jewelryModel.setMeta_title(meta_title);
                            jewelryModel.setMeta_description(meta_description);
                            jewelryModel.setMeta_img(meta_img);
                            jewelryModel.setPdf(pdf);
                            jewelryModel.setSlug(slug);
                            jewelryModel.setRating(rating);
                            jewelryModel.setCreated_at(created_at);
                            jewelryModel.setUpdated_at(updated_at);

                            jewelryArrayList.add(jewelryModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        jewelrycategoryRecyclerView.setLayoutManager(layoutManager);

                        jewelryProductGridAdapter = new JewelryProductGridAdapter(context, jewelryArrayList);
                        jewelryProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedJewelry = jewelryArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedJewelry.getName());
                                intent.putExtra("ITEMPRICE", selectedJewelry.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedJewelry.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedJewelry.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedJewelry.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        jewelrycategoryRecyclerView.setAdapter(jewelryProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }
}
