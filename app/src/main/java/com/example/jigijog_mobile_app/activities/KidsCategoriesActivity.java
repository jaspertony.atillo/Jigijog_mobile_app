package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.ComputerProductAdapter;
import com.example.jigijog_mobile_app.adapters.KidsProductAdapter;
import com.example.jigijog_mobile_app.adapters.KidsProductGridAdapter;
import com.example.jigijog_mobile_app.adapters.WomensProductAdapter;
import com.example.jigijog_mobile_app.adapters.WomensProductGridAdapter;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Computer;
import com.example.jigijog_mobile_app.models.Kids;
import com.example.jigijog_mobile_app.models.Women;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.Tools;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class KidsCategoriesActivity extends AppCompatActivity {

    private View view;
    private Context context;
    private String message;

    private ImageView iv_kidscategoryBack, iv_kidscategoryGrid,iv_kidscategoryMore,iv_kidscategoryCart;
    private TextView tv_kidscategoryName;

    private ConstraintLayout emptyIndicator;


    private RecyclerView kidscategoryRecyclerView;
    private ArrayList<Kids> kidsArrayList;
    private KidsProductAdapter kidsProductAdapter;
    private KidsProductGridAdapter kidsProductGridAdapter;

    private Kids selectedKids = new Kids();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kids_categories);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = getIntent();
        message = intent.getStringExtra("SUBCATEGORY");
        initializeUI();
        if(message.equals("Action Figures")) {
            tv_kidscategoryName.setText( message);
            loadKidsActionFigures();
            iv_kidscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadKidsActionFiguresGrid();
                }
            });
        }else if (message.equals("Arts & Crafts")){
            tv_kidscategoryName.setText( message);
            loadKidsArtsandCraft();
            iv_kidscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadKidsArtsandCraftGrid();
                }
            });
        }else if (message.equals("Battling Toys")){
            tv_kidscategoryName.setText( message);
            loadKidsBattlingToys();
            iv_kidscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadKidsBattlingToysGrid();
                }
            });
        }else if (message.equals("Building & Construction")){
            tv_kidscategoryName.setText( message);
            loadKidsBuildingConstruction();
            iv_kidscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadKidsBuildingConstructionGrid();
                }
            });
        }
        else if (message.equals("Collection Trading Cards & Toys")){
            tv_kidscategoryName.setText( message);
            loadKidsTradingCards();
            iv_kidscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadKidsTradingCardsGrid();
                }
            });
        }
        else if (message.equals("Costume & Dress-up")){
            tv_kidscategoryName.setText( message);
            loadKidsCostume();
            iv_kidscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadKidsCostumeGrid();
                }
            });
        }
        else if (message.equals("Dolls")){
            tv_kidscategoryName.setText( message);
            loadKidsDolls();
            iv_kidscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadKidsDollsGrid();
                }
            });
        }
        else if (message.equals("Educational")){
            tv_kidscategoryName.setText( message);
            loadKidsEducational();
            iv_kidscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadKidsEducationalGrid();
                }
            });
        }
        else if (message.equals("Games & Puzzles")){
            tv_kidscategoryName.setText( message);
            loadKidsGamesPuzzles();
            iv_kidscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadKidsGamesPuzzlesGrid();
                }
            });
        }
        else if (message.equals("Lingerie, Sleep & Lounge")){
            tv_kidscategoryName.setText( message);
            loadKidsLounge();
            iv_kidscategoryGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadKidsLoungeGrid();
                }
            });
        }

    }


    private void initializeUI() {
        emptyIndicator = findViewById(R.id.view_Empty);
        tv_kidscategoryName = findViewById(R.id.tv_kidscategoryName);
        iv_kidscategoryGrid = findViewById(R.id.iv_kidscategoryGrid);
        iv_kidscategoryBack = findViewById(R.id.iv_kidscategoryBack);
        iv_kidscategoryCart = findViewById(R.id.iv_kidscategoryCart);
        iv_kidscategoryCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CartActivity.class);
                startActivity(intent);
            }
        });
        iv_kidscategoryMore = findViewById(R.id.iv_kidscategoryMore);
        iv_kidscategoryBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_kidscategoryMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, MyAccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    private void loadKidsActionFigures(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllActionFigures", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductAdapter = new KidsProductAdapter(context, kidsArrayList);
                        kidsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }
    private void loadKidsArtsandCraft(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllArtsAndCrafts", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductAdapter = new KidsProductAdapter(context, kidsArrayList);
                        kidsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsBattlingToys(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllBattlingToys", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductAdapter = new KidsProductAdapter(context, kidsArrayList);
                        kidsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsBuildingConstruction(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllBuildingAndConstruction", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductAdapter = new KidsProductAdapter(context, kidsArrayList);
                        kidsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsTradingCards(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllCollectibleTradingCardsAndToys", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductAdapter = new KidsProductAdapter(context, kidsArrayList);
                        kidsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsCostume(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllCostumeAndDressUp", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductAdapter = new KidsProductAdapter(context, kidsArrayList);
                        kidsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsDolls(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllDolls", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductAdapter = new KidsProductAdapter(context, kidsArrayList);
                        kidsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsEducational(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllEducational", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductAdapter = new KidsProductAdapter(context, kidsArrayList);
                        kidsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsGamesPuzzles(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllGamesAndPuzzles", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductAdapter = new KidsProductAdapter(context, kidsArrayList);
                        kidsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsLounge(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllLingerieSleepAndLounge", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductAdapter = new KidsProductAdapter(context, kidsArrayList);
                        kidsProductAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void loadKidsActionFiguresGrid(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllActionFigures", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductGridAdapter = new KidsProductGridAdapter(context, kidsArrayList);
                        kidsProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsArtsandCraftGrid(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllArtsAndCrafts", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductGridAdapter = new KidsProductGridAdapter(context, kidsArrayList);
                        kidsProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsBattlingToysGrid(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllBattlingToys", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductGridAdapter = new KidsProductGridAdapter(context, kidsArrayList);
                        kidsProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsBuildingConstructionGrid(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllBuildingAndConstruction", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductGridAdapter = new KidsProductGridAdapter(context, kidsArrayList);
                        kidsProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsTradingCardsGrid(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllCollectibleTradingCardsAndToys", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductGridAdapter = new KidsProductGridAdapter(context, kidsArrayList);
                        kidsProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsCostumeGrid(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllCostumeAndDressUp", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductGridAdapter = new KidsProductGridAdapter(context, kidsArrayList);
                        kidsProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsDollsGrid(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllDolls", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductGridAdapter = new KidsProductGridAdapter(context, kidsArrayList);
                        kidsProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsEducationalGrid(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllEducational", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductGridAdapter = new KidsProductGridAdapter(context, kidsArrayList);
                        kidsProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsGamesPuzzlesGrid(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllGamesAndPuzzles", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductGridAdapter = new KidsProductGridAdapter(context, kidsArrayList);
                        kidsProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }
    private void loadKidsLoungeGrid(){
        kidscategoryRecyclerView = findViewById(R.id.kidscategoryRecyclerView);
        kidsArrayList = new ArrayList<>();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getAllLingerieSleepAndLounge", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                    emptyIndicator.setVisibility(View.VISIBLE);
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");
                            //ArrayList<String> shit = jsonObject.getString("shit");

                            Kids kidsModel = new Kids();
                            kidsModel.setId(id);
                            kidsModel.setName(name);
                            kidsModel.setAdded_by(added_by);
                            kidsModel.setCategory_id(category_id);
                            kidsModel.setSubcategory_id(subcategory_id);
                            kidsModel.setSubsubcategory_id(subsubcategory_id);
                            kidsModel.setBrand_id(brand_id);
                            kidsModel.setPhotos(photos);
                            kidsModel.setThumbnail_img(thumbnail_img);
                            kidsModel.setFeatured_img(featured_img);
                            kidsModel.setFlash_deal_img(flash_deal_img);
                            kidsModel.setVideo_provider(video_provider);
                            kidsModel.setVideo_link(video_link);
                            kidsModel.setTags(tags);
                            kidsModel.setDescription(description);
                            kidsModel.setUnit_price(unit_price);
                            kidsModel.setPurchase_price(purchase_price);
                            kidsModel.setChoice_options(choice_options);
                            kidsModel.setColors(colors);
                            kidsModel.setVariations(variations);
                            kidsModel.setTodays_deal(todays_deal);
                            kidsModel.setPublished(published);
                            kidsModel.setFeatured(featured);
                            kidsModel.setCurrent_stock(current_stock);
                            kidsModel.setUnit(unit);
                            kidsModel.setDiscount(discount);
                            kidsModel.setDiscount_type(discount_type);
                            kidsModel.setTax(tax);
                            kidsModel.setTax_type(tax_type);
                            kidsModel.setShipping_type(shipping_type);
                            kidsModel.setShipping_cost(shipping_cost);
                            kidsModel.setWeight(weight);
                            kidsModel.setParcel_size(parcel_size);
                            kidsModel.setNum_of_sale(num_of_sale);
                            kidsModel.setMeta_title(meta_title);
                            kidsModel.setMeta_description(meta_description);
                            kidsModel.setMeta_img(meta_img);
                            kidsModel.setPdf(pdf);
                            kidsModel.setSlug(slug);
                            kidsModel.setRating(rating);
                            kidsModel.setCreated_at(created_at);
                            kidsModel.setUpdated_at(updated_at);

                            kidsArrayList.add(kidsModel);
                        }

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
                        kidscategoryRecyclerView.setLayoutManager(layoutManager);

                        kidsProductGridAdapter = new KidsProductGridAdapter(context, kidsArrayList);
                        kidsProductGridAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {
                                selectedKids = kidsArrayList.get(position);

                                Bundle extras = new Bundle();
                                Intent intent = new Intent(context, ProductViewActivity.class);
                                intent.putExtra("ITEMNAME", selectedKids.getName());
                                intent.putExtra("ITEMPRICE", selectedKids.getUnit_price());
                                intent.putExtra("ITEMDISPRICE", selectedKids.getDiscount());
                                intent.putExtra("ITEMIMAGE",selectedKids.getFeatured_img());
                                intent.putExtra("ITEMIDES",selectedKids.getDescription());
                                intent.putExtras(extras);
                                startActivity(intent);
                            }
                        });
                        kidscategoryRecyclerView.setAdapter(kidsProductGridAdapter);

                        emptyIndicator.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

}
