package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;

import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.FeaturedAdapter;
import com.example.jigijog_mobile_app.adapters.FeaturedGridAdapter;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Featured;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.Tools;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

import cz.msebera.android.httpclient.Header;

public class FeaturedProductsActivity extends AppCompatActivity {
    private View view;
    private Context context;

    private ImageView iv_featuredBack;
    private ImageView iv_featuredGrid,iv_featuredMore,iv_featuredCart;

    private ConstraintLayout emptyIndicator;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView featuredRecyclerview;
    private ArrayList<Featured> featuredArrayList;
    private FeaturedAdapter featuredAdapter;
    private FeaturedGridAdapter featuredGridAdapter;

    private Featured selectedFeatured = new Featured();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_featured_products);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = getApplicationContext();

    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeUI();
        loadFeaturedProducts();
    }

    private void initializeUI () {
        iv_featuredBack = findViewById(R.id.iv_featuredBack);
        iv_featuredGrid = findViewById(R.id.iv_featuredGrid);
        swipeRefreshLayout = findViewById(R.id.swipe_Home);
        iv_featuredMore = findViewById(R.id.iv_featuredMore);
        iv_featuredCart = findViewById(R.id.iv_featuredCart);
        iv_featuredCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CartActivity.class);
                startActivity(intent);
            }
        });
        swipeRefreshLayout.setRefreshing(false);

        iv_featuredGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFeaturedProductsGrid();
            }
        });
        iv_featuredBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        iv_featuredMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, MyAccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    private void loadFeaturedProducts() {
        swipeRefreshLayout.setRefreshing(true);

        featuredRecyclerview = findViewById(R.id.featuredRecyclerview);
        featuredArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getFeaturedProducts", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                try {
                    swipeRefreshLayout.setRefreshing(false);
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");

                        Featured featuredModel = new Featured();
                        featuredModel.setId(id);
                        featuredModel.setName(name);
                        featuredModel.setAdded_by(added_by);
                        featuredModel.setCategory_id(category_id);
                        featuredModel.setSubcategory_id(subcategory_id);
                        featuredModel.setSubsubcategory_id(subsubcategory_id);
                        featuredModel.setBrand_id(brand_id);
                        featuredModel.setPhotos(photos);
                        featuredModel.setThumbnail_img(thumbnail_img);
                        featuredModel.setFeatured_img(featured_img);
                        featuredModel.setFlash_deal_img(flash_deal_img);
                        featuredModel.setVideo_provider(video_provider);
                        featuredModel.setVideo_link(video_link);
                        featuredModel.setTags(tags);
                        featuredModel.setDescription(description);
                        featuredModel.setUnit_price(unit_price);
                        featuredModel.setPurchase_price(purchase_price);
                        featuredModel.setChoice_options(choice_options);
                        featuredModel.setColors(colors);
                        featuredModel.setVariations(variations);
                        featuredModel.setTodays_deal(todays_deal);
                        featuredModel.setPublished(published);
                        featuredModel.setFeatured(featured);
                        featuredModel.setCurrent_stock(current_stock);
                        featuredModel.setUnit(unit);
                        featuredModel.setDiscount(discount);
                        featuredModel.setDiscount_type(discount_type);
                        featuredModel.setTax(tax);
                        featuredModel.setTax_type(tax_type);
                        featuredModel.setShipping_type(shipping_type);
                        featuredModel.setShipping_cost(shipping_cost);
                        featuredModel.setWeight(weight);
                        featuredModel.setParcel_size(parcel_size);
                        featuredModel.setNum_of_sale(num_of_sale);
                        featuredModel.setMeta_title(meta_title);
                        featuredModel.setMeta_description(meta_description);
                        featuredModel.setMeta_img(meta_img);
                        featuredModel.setPdf(pdf);
                        featuredModel.setSlug(slug);
                        featuredModel.setRating(rating);
                        featuredModel.setCreated_at(created_at);
                        featuredModel.setUpdated_at(updated_at);

                        featuredArrayList.add(featuredModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                    featuredRecyclerview.setLayoutManager(layoutManager);

                    featuredAdapter = new FeaturedAdapter(context, featuredArrayList);
                    featuredAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedFeatured = featuredArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMNAME", selectedFeatured.getName());
                            intent.putExtra("ITEMPRICE", selectedFeatured.getUnit_price());
                            intent.putExtra("ITEMDISPRICE", selectedFeatured.getDiscount());
                            intent.putExtra("ITEMIMAGE",selectedFeatured.getFeatured_img());
                            intent.putExtra("ITEMIDES",selectedFeatured.getDescription());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    featuredRecyclerview.setAdapter(featuredAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });


    }

    private void loadFeaturedProductsGrid() {
        swipeRefreshLayout.setRefreshing(true);

        featuredRecyclerview = findViewById(R.id.featuredRecyclerview);
        featuredArrayList = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://jigijog.com/v1/mobile-api/api/mobile/getFeaturedProducts", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String str = new String(response, StandardCharsets.UTF_8);
                try {
                    swipeRefreshLayout.setRefreshing(false);
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i <= jsonArray.length() - 1; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String name = jsonObject.getString("name");
                        String added_by = jsonObject.getString("added_by");
                        String category_id = jsonObject.getString("category_id");
                        String subcategory_id = jsonObject.getString("subcategory_id");
                        String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                        String brand_id = jsonObject.getString("brand_id");
                        String photos = jsonObject.getString("photos");
                        String thumbnail_img = jsonObject.getString("thumbnail_img");
                        String featured_img = jsonObject.getString("featured_img");
                        String flash_deal_img = jsonObject.getString("flash_deal_img");
                        String video_provider = jsonObject.getString("video_provider");
                        String video_link = jsonObject.getString("video_link");
                        String tags = jsonObject.getString("tags");
                        String description = jsonObject.getString("description");
                        String unit_price = jsonObject.getString("unit_price");
                        String purchase_price = jsonObject.getString("purchase_price");
                        String choice_options = jsonObject.getString("choice_options");
                        String colors = jsonObject.getString("colors");
                        String variations = jsonObject.getString("variations");
                        String todays_deal = jsonObject.getString("todays_deal");
                        String published = jsonObject.getString("published");
                        String featured = jsonObject.getString("featured");
                        String current_stock = jsonObject.getString("current_stock");
                        String unit = jsonObject.getString("unit");
                        String discount = jsonObject.getString("discount");
                        String discount_type = jsonObject.getString("discount_type");
                        String tax = jsonObject.getString("tax");
                        String tax_type = jsonObject.getString("tax_type");
                        String shipping_type = jsonObject.getString("shipping_type");
                        String shipping_cost = jsonObject.getString("shipping_cost");
                        String weight = jsonObject.getString("weight");
                        String parcel_size = jsonObject.getString("parcel_size");
                        String num_of_sale = jsonObject.getString("num_of_sale");
                        String meta_title = jsonObject.getString("meta_title");
                        String meta_description = jsonObject.getString("meta_description");
                        String meta_img = jsonObject.getString("meta_img");
                        String pdf = jsonObject.getString("pdf");
                        String slug = jsonObject.getString("slug");
                        String rating = jsonObject.getString("rating");
                        String created_at = jsonObject.getString("created_at");
                        String updated_at = jsonObject.getString("updated_at");

                        Featured featuredModel = new Featured();
                        featuredModel.setId(id);
                        featuredModel.setName(name);
                        featuredModel.setAdded_by(added_by);
                        featuredModel.setCategory_id(category_id);
                        featuredModel.setSubcategory_id(subcategory_id);
                        featuredModel.setSubsubcategory_id(subsubcategory_id);
                        featuredModel.setBrand_id(brand_id);
                        featuredModel.setPhotos(photos);
                        featuredModel.setThumbnail_img(thumbnail_img);
                        featuredModel.setFeatured_img(featured_img);
                        featuredModel.setFlash_deal_img(flash_deal_img);
                        featuredModel.setVideo_provider(video_provider);
                        featuredModel.setVideo_link(video_link);
                        featuredModel.setTags(tags);
                        featuredModel.setDescription(description);
                        featuredModel.setUnit_price(unit_price);
                        featuredModel.setPurchase_price(purchase_price);
                        featuredModel.setChoice_options(choice_options);
                        featuredModel.setColors(colors);
                        featuredModel.setVariations(variations);
                        featuredModel.setTodays_deal(todays_deal);
                        featuredModel.setPublished(published);
                        featuredModel.setFeatured(featured);
                        featuredModel.setCurrent_stock(current_stock);
                        featuredModel.setUnit(unit);
                        featuredModel.setDiscount(discount);
                        featuredModel.setDiscount_type(discount_type);
                        featuredModel.setTax(tax);
                        featuredModel.setTax_type(tax_type);
                        featuredModel.setShipping_type(shipping_type);
                        featuredModel.setShipping_cost(shipping_cost);
                        featuredModel.setWeight(weight);
                        featuredModel.setParcel_size(parcel_size);
                        featuredModel.setNum_of_sale(num_of_sale);
                        featuredModel.setMeta_title(meta_title);
                        featuredModel.setMeta_description(meta_description);
                        featuredModel.setMeta_img(meta_img);
                        featuredModel.setPdf(pdf);
                        featuredModel.setSlug(slug);
                        featuredModel.setRating(rating);
                        featuredModel.setCreated_at(created_at);
                        featuredModel.setUpdated_at(updated_at);

                        featuredArrayList.add(featuredModel);
                    }
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                    featuredRecyclerview.setLayoutManager(layoutManager);

                    featuredGridAdapter = new FeaturedGridAdapter(context, featuredArrayList);
                    featuredGridAdapter.setClickListener(new OnClickRecyclerView() {
                        @Override
                        public void onItemClick(View view, int position) {
                            selectedFeatured = featuredArrayList.get(position);
                            Bundle extras = new Bundle();
                            Intent intent = new Intent(context, ProductViewActivity.class);
                            intent.putExtra("ITEMNAME", selectedFeatured.getName());
                            intent.putExtra("ITEMPRICE", selectedFeatured.getUnit_price());
                            intent.putExtra("ITEMDISPRICE", selectedFeatured.getDiscount());
                            intent.putExtra("ITEMIMAGE",selectedFeatured.getFeatured_img());
                            intent.putExtra("ITEMIDES",selectedFeatured.getDescription());
                            intent.putExtras(extras);
                            startActivity(intent);
                        }
                    });
                    featuredRecyclerview.setAdapter(featuredGridAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Debugger.logD("shit " + e.toString());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Debugger.logD("shit loadTodaysDeal " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }
}
