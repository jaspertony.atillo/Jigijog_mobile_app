package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.utils.Tools;

public class DeliveryDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_detail);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);    }
}
