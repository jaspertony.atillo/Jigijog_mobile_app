package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.utils.Tools;

public class MyAccountActivity extends AppCompatActivity {
    private View view;
    private Context context;

    private ImageView iv_accountSetting,iv_accountBack,iv_accountReturns,iv_accountCancel;
    private TextView tv_accountName, tv_myorders;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        Tools.setSystemBarColor(this   , R.color.deep_orange_400);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        initializeUI();
    }

    private void initializeUI () {
        iv_accountSetting = findViewById(R.id.iv_accountSetting);
        iv_accountBack = findViewById(R.id.iv_accountBack);
        tv_accountName = findViewById(R.id.tv_accountName);
        tv_myorders = findViewById(R.id.tv_myorders);
        iv_accountReturns = findViewById(R.id.iv_accountReturns);
        iv_accountCancel = findViewById(R.id.iv_accountCancel);
        iv_accountReturns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ReturnCancelActivity.class);
                startActivity(intent);
            }
        });
        iv_accountCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ReturnCancelActivity.class);
                startActivity(intent);
            }
        });
        tv_myorders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MyOrdersActivity.class);
                startActivity(intent);
            }
        });
        tv_accountName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
            }
        });
        iv_accountBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_accountSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SettingsActivity.class);
                startActivity(intent);
            }
        });
    }
}
