package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.jigijog_mobile_app.HttpProvider;
import com.example.jigijog_mobile_app.MainActivity;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.CustomSwipeAdapter;
import com.example.jigijog_mobile_app.adapters.CustomSwipeAdapter2;
import com.example.jigijog_mobile_app.adapters.NewArrivalAdapter;
import com.example.jigijog_mobile_app.adapters.RelatedProductsAdapter;
import com.example.jigijog_mobile_app.adapters.SportsProductAdapter;
import com.example.jigijog_mobile_app.adapters.TopSellingPerSellerAdapter;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Products;
import com.example.jigijog_mobile_app.models.RelatedProducts;
import com.example.jigijog_mobile_app.models.Slide;
import com.example.jigijog_mobile_app.models.Sports;
import com.example.jigijog_mobile_app.models.TopSelling;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.Tools;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class ProductViewActivity extends AppCompatActivity {

    private View view;
    private Context context;
    private ImageView iv_productItemBack, iv_productItemCart, iv_productItemMore, tv_productItemImage, iv_productItemHeart,iv_productItemShare;
    private TextView tv_productItemItemName, tv_productItemPrice, tv_productItemDisPrice,tv_description,tv_descriptionDis,tv_productcategoryCart,tv_deliveriesDis,tv_productItemStock;
    private Button btn_BuyNow, btn_AddToCart;

    private String itemName, itemPrice, itemDisPrice,itemImage, itemDes, itemID;
    private int productID = 0;
    private ViewPager viewPager;

    private ArrayList<Products> productsArrayList;
    private CustomSwipeAdapter2 customSwipeAdapter2;
    private LinearLayout dotsLayout;
    private int custom_pos = 0;

    private Timer timer;
    private int current_pos = 0;
    private ArrayList<Products.Photos> photosArrayList;
    private ArrayList<Products.Choice_Options> choice_optionsArrayList;
    private ArrayList<Products.Colors> colorsArrayList;
    private ArrayList<Products.Variations> variationsArrayList;
    private int cartCount = 0;

    private RecyclerView topSellingRecyclerView;
    private TopSellingPerSellerAdapter topSellingPerSellerAdapter;
    private ArrayList<TopSelling> topSellingArrayList;

    private ArrayList<TopSelling.Photos> topSeelingPhotosArrayList;
    private ArrayList<TopSelling.Choice_Options> topSellingChoice_optionsArrayList;
    private ArrayList<TopSelling.Colors> topSellingColorsArrayList;
    private ArrayList<TopSelling.Variations> topSellingVariationsArrayList;
    private Products productsModel;


    private RecyclerView relatedProdRecyclerView;
    private RelatedProductsAdapter relatedProductsAdapter;
    private ArrayList<RelatedProducts> relatedProductsArrayList;

    private ArrayList<RelatedProducts.Photos> relatedProdPhotosArrayList;
    private ArrayList<RelatedProducts.Choice_Options> relatedProdChoice_optionsArrayList;
    private ArrayList<RelatedProducts.Colors> relatedProdColorsArrayList;
    private ArrayList<RelatedProducts.Variations> relatedProdVariationsArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_view);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = getIntent();
        productID = Integer.parseInt(itemID = intent.getStringExtra("ITEMID"));
        itemName = intent.getStringExtra("ITEMNAME");
        Debugger.logD(""+ productID);
        initializeUI();
        loadProductItemByID();

    }

    private void initializeUI() {
        viewPager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.dotsContainer);
        tv_productcategoryCart = findViewById(R.id.tv_productcategoryCart);
        iv_productItemBack = findViewById(R.id.iv_productItemBack);
        iv_productItemCart = findViewById(R.id.iv_productItemCart);
        iv_productItemMore = findViewById(R.id.iv_productItemMore);
//        tv_productItemImage = findViewById(R.id.tv_productItemImage);
        iv_productItemHeart = findViewById(R.id.iv_productItemHeart);
        iv_productItemShare = findViewById(R.id.iv_productItemShare);
        tv_productItemItemName = findViewById(R.id.tv_productItemItemName);
        tv_productItemPrice = findViewById(R.id.tv_productItemPrice);
        tv_productItemDisPrice = findViewById(R.id.tv_productItemDisPrice);
        tv_description = findViewById(R.id.tv_description);
        btn_BuyNow = findViewById(R.id.btn_BuyNow);
        btn_AddToCart = findViewById(R.id.btn_AddToCart);
        tv_descriptionDis = findViewById(R.id.tv_descriptionDis);
        tv_deliveriesDis = findViewById(R.id.tv_deliveriesDis);
        tv_productItemStock = findViewById(R.id.tv_productItemStock);
        iv_productItemBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_BuyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.warning(context, "Button Buy Now is Clicked").show();
            }
        });

        btn_AddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cartCount++;
                tv_productcategoryCart.setVisibility(View.VISIBLE);
                tv_productcategoryCart.setText(String.valueOf(cartCount));
                productsModel.getId();
            }
        });
        iv_productItemCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,CartActivity.class);
                intent.putExtra("PRODUCTID",productsModel.getId());
                startActivity(intent);
            }
        });
        iv_productItemMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,view);
                popupMenu.inflate(R.menu.side_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.home:
                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                finish();
                                break;
                            case R.id.message:
                                Intent intent1 = new Intent(context, MessageActivity.class);
                                startActivity(intent1);
                                finish();
                                break;
                            case R.id.myaccount:
                                Intent intent2 = new Intent(context, MyAccountActivity.class);
                                startActivity(intent2);
                                finish();
                                break;
                            case R.id.help:
                                Intent intent3 = new Intent(context, HelpActivity.class);
                                startActivity(intent3);
                                finish();
                                break;
                        }

                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    private void loadProductItemByID(){
        viewPager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.dotsContainer);
        productsArrayList = new ArrayList<>();
        RequestParams params = new RequestParams();
        params.put("product_id", 1111);
        Debugger.logD("asd "+ productID);
        HttpProvider.post(context, "/mobile/selectedProductById", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String user_id = jsonObject.getString("user_id");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");

                            productsModel = new Products();

                            photosArrayList = new ArrayList<>();
                            JSONArray jsonArrays = new JSONArray(Arrays.asList(photos));

                            for(int z=0; z<jsonArrays.length(); z++) {
                                JSONObject jobject = jsonArray.getJSONObject(z);

                                String photos2 = jobject.getString("photos");

                                //photos2 = photos2.replace("\\\"","'");
                                JSONArray jsonArrayssss = new JSONArray(photos2);
                                Debugger.logD("jsonArrayssss  " + jsonArrayssss);
                                if(jsonArrayssss.length() > 0){
                                    for (int h = 0; h < jsonArrayssss.length(); h++) {
                                        Debugger.logD( "fucksss " + jsonArrayssss.optString(h));

                                        Products.Photos photos1 = new Products.Photos();
                                        photos1.setPhoto(jsonArrayssss.optString(h));

                                        photosArrayList.add(photos1);
                                        JSONArray childJsonArray=jsonArrayssss.optJSONArray(h);
                                        if(childJsonArray!=null && childJsonArray.length()>0){
                                            for (int w = 0; w < childJsonArray.length(); w++) {
                                                Debugger.logD( "fuck " + childJsonArray.optString(w));
                                                String sss = childJsonArray.optString(w);
                                                String results = sss.substring(2, photos2.length() -2).replace("\\","");

                                                Debugger.logD( "sss " + sss);
                                                Products.Photos photos1s = new Products.Photos();
                                                photos1s.setPhoto(childJsonArray.optString(w));

                                                photosArrayList.add(photos1s);


                                            }
                                        }
                                        customSwipeAdapter2 = new CustomSwipeAdapter2(context, photosArrayList);
                                        viewPager.setAdapter(customSwipeAdapter2);
                                        loadPreparedDots(custom_pos++);
                                        //loadCreateSlideShow();
                                        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                            @Override
                                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                                            }

                                            @Override
                                            public void onPageSelected(int position) {
                                                if (custom_pos>photosArrayList.size()){
                                                    custom_pos = 0;
                                                }
                                                loadPreparedDots(custom_pos++);
                                            }

                                            @Override
                                            public void onPageScrollStateChanged(int state) {

                                            }
                                        });

                                    }
                                }


//                                Debugger.logD("jsonArrayssss  " + jsonArrayssss);
//                                for (int s = 0; s < jsonArrayssss.length(); s++) {
//
//                                }


                                //jsonarray to jsonobject
                                JSONObject jos = new JSONObject();

                                jos.put("photos", jsonArrayssss);

                                Debugger.logD("jo  " + jos);

                                JSONObject resobj = new JSONObject(String.valueOf(jos));
                                Debugger.logD("resobj  " + resobj);
//                                for (int s = 0; s < resobj.length(); s++) {
//                                    String desc = resobj.getString("photos");
//
//                                    Debugger.logD("desc  " + desc);
//                                }


//                                JSONArray jsonarrays = new JSONArray(jsonArrayssss);
//                                for(int t=0; t < jsonarrays.length(); t++) {
//                                    JSONObject jsonobjectssss = jsonArrayssss.getJSONObject(t);
//                                    Debugger.logD("jsonobjectssss  " + jsonobjectssss);
//                                }
//                                JSONObject jsonObjects = new JSONObject(photos2);
//                                Debugger.logD("jsonObjects  " + jsonObjects);
//
//                                Debugger.logD("hey " + photos2);
//                                JSONArray jsonArr = new JSONArray(photos2);
//                                JSONObject jo = new JSONObject(photos2.substring(1,photos2.length()-1));
//
//                                Debugger.logD("hey " + jo);
//                                for (int x = 0; x < jsonArr.length(); x++)
//                                {
//                                    JSONObject jsonObj = jsonArr.getJSONObject(x);
//
//                                    Debugger.logD("hey " + jsonObj);
//                                }

                                //Debugger.logD(photos2);



                            }

//                            choice_optionsArrayList = new ArrayList<>();
//                            JSONArray jsonArray2 = new JSONArray(Arrays.asList(choice_options));
//                            for (int k = 0; k <= jsonArray2.length() - 1; k++) {
//                                JSONObject jsonObject2 = jsonArray2.getJSONObject(k);
//                                String choice_options2 = jsonObject2.getString("choice_options");
//
//                                Products.Choice_Options choice_options1 = new Products.Choice_Options();
//                                choice_options1.setChoice_option(choice_options2);
//
//                                choice_optionsArrayList.add(choice_options1);
//                            }
//                            colorsArrayList = new ArrayList<>();
//                            JSONArray jsonArray3 = new JSONArray(Arrays.asList(colors));
//                            for (int l = 0; l <= jsonArray2.length() - 1; l++) {
//                                JSONObject jsonObject2 = jsonArray3.getJSONObject(l);
//                                String colors2 = jsonObject2.getString("colors");
//
//                                Products.Colors colors1 = new Products.Colors();
//                                colors1.setColor(colors2);
//
//                                colorsArrayList.add(colors1);
//                            }
//
//                            variationsArrayList = new ArrayList<>();
//                            JSONArray jsonArray4 = new JSONArray(Arrays.asList(variations));
//                            for (int n = 0; n <= jsonArray2.length() - 1; n++) {
//                                JSONObject jsonObject2 = jsonArray4.getJSONObject(n);
//                                String variations2 = jsonObject2.getString("variations");
//
//                                Products.Variations variations1 = new Products.Variations();
//                                variations1.setVariation(variations2);
//
//                                variationsArrayList.add(variations1);
//                            }

                            productsModel.setId(id);
                            productsModel.setName(name);
                            productsModel.setAdded_by(added_by);
                            productsModel.setUser_id(user_id);
                            productsModel.setCategory_id(category_id);
                            productsModel.setSubcategory_id(subcategory_id);
                            productsModel.setSubsubcategory_id(subsubcategory_id);
                            productsModel.setBrand_id(brand_id);
                            //productsModel.setPhotos(photos);
                            productsModel.setThumbnail_img(thumbnail_img);
                            productsModel.setFeatured_img(featured_img);
                            productsModel.setFlash_deal_img(flash_deal_img);
                            productsModel.setVideo_provider(video_provider);
                            productsModel.setVideo_link(video_link);
                            productsModel.setTags(tags);
                            productsModel.setDescription(description);
                            productsModel.setUnit_price(unit_price);
                            productsModel.setPurchase_price(purchase_price);
                            //productsModel.setChoice_options(choice_options);
                           // productsModel.setColors(colors);
                            //productsModel.setVariations(variations);
                            productsModel.setTodays_deal(todays_deal);
                            productsModel.setPublished(published);
                            productsModel.setFeatured(featured);
                            productsModel.setCurrent_stock(current_stock);
                            productsModel.setUnit(unit);
                            productsModel.setDiscount(discount);
                            productsModel.setDiscount_type(discount_type);
                            productsModel.setTax(tax);
                            productsModel.setTax_type(tax_type);
                            productsModel.setShipping_type(shipping_type);
                            productsModel.setShipping_cost(shipping_cost);
                            productsModel.setWeight(weight);
                            productsModel.setParcel_size(parcel_size);
                            productsModel.setNum_of_sale(num_of_sale);
                            productsModel.setMeta_title(meta_title);
                            productsModel.setMeta_description(meta_description);
                            productsModel.setMeta_img(meta_img);
                            productsModel.setPdf(pdf);
                            productsModel.setSlug(slug);
                            productsModel.setRating(rating);
                            productsModel.setCreated_at(created_at);
                            productsModel.setUpdated_at(updated_at);

                            productsArrayList.add(productsModel);

                            tv_productItemPrice.setText("\u20B1" + unit_price + ".00");
                            tv_productItemDisPrice.setText(discount);
                            tv_productItemDisPrice.setPaintFlags(tv_productItemDisPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            if (name.length() <= 24){
                                tv_productItemItemName.setText(name);
                            }else{
                                tv_productItemItemName.setText((name).substring(0, 24) + ".." );
                            }

                            tv_descriptionDis.setText(" " + description);
                            tv_deliveriesDis.setText("Fulfilled by Jigijog available - " + shipping_type );
                            tv_productItemStock.setText("( " +current_stock + " available )" );

//                            if (current_stock.equals("0")){
//                                btn_AddToCart.setEnabled(false);
//                                btn_AddToCart.setBackgroundColor(Color.LTGRAY);
//                                btn_AddToCart.setTextColor(Color.BLACK);
//                            }else{
//                                btn_AddToCart.setEnabled(true);
//                            }

                            loadTopSellingPerSeller(Integer.parseInt(productsModel.getUser_id()));
                            loadRelatedProducts(Integer.parseInt(productsModel.getCategory_id()));

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    private void loadCreateSlideShow(){
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (current_pos == Integer.MAX_VALUE) {
                    current_pos = 0;
                }
                viewPager.setCurrentItem(current_pos++, true);
            }
        };

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);

            }
        },250, 2500);

    }
    private void loadPreparedDots(int currentSlidePos){
        if(dotsLayout.getChildCount()>0){
            dotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[photosArrayList.size()];
        for (int i =0; i<photosArrayList.size();i++){
            dots[i] = new ImageView(context);
            if(i==currentSlidePos){
                dots[i].setImageDrawable(ContextCompat.getDrawable(context,R.drawable.active_dot));
            }else{
                dots[i].setImageDrawable(ContextCompat.getDrawable(context,R.drawable.inactive_dots));
            }
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            layoutParams.setMargins(4, 0, 4,0);
            dotsLayout.addView(dots[i],layoutParams);
        }
    }


    private void loadTopSellingPerSeller(int user_ids){
        topSellingArrayList = new ArrayList<>();
        topSellingRecyclerView = findViewById(R.id.topSellingRecyclerView);

        RequestParams params = new RequestParams();
        params.put("user_id", user_ids);
        Debugger.logD("user_ids "+ user_ids);
        HttpProvider.post(context, "/mobile/topSellingPerSeller", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String user_id = jsonObject.getString("user_id");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");

                            TopSelling topSellingModel = new TopSelling();

                            topSeelingPhotosArrayList = new ArrayList<>();
                            JSONArray jsonArrays = new JSONArray(Arrays.asList(photos));

                            for(int z=0; z<jsonArrays.length(); z++) {
                                JSONObject jobject = jsonArray.getJSONObject(z);

                                String photos2 = jobject.getString("photos");

                                String result = photos2.substring(2, photos2.length() -2).replace("\\","");

                                TopSelling.Photos photos1 = new TopSelling.Photos();
                                photos1.setPhoto(result);

                                topSeelingPhotosArrayList.add(photos1);

                            }

                            topSellingModel.setId(id);
                            topSellingModel.setName(name);
                            topSellingModel.setAdded_by(added_by);
                            topSellingModel.setUser_id(user_id);
                            topSellingModel.setCategory_id(category_id);
                            topSellingModel.setSubcategory_id(subcategory_id);
                            topSellingModel.setSubsubcategory_id(subsubcategory_id);
                            topSellingModel.setBrand_id(brand_id);
                            //topSellingModel.setPhotos(photos);
                            topSellingModel.setThumbnail_img(thumbnail_img);
                            topSellingModel.setFeatured_img(featured_img);
                            topSellingModel.setFlash_deal_img(flash_deal_img);
                            topSellingModel.setVideo_provider(video_provider);
                            topSellingModel.setVideo_link(video_link);
                            topSellingModel.setTags(tags);
                            topSellingModel.setDescription(description);
                            topSellingModel.setUnit_price(unit_price);
                            topSellingModel.setPurchase_price(purchase_price);
                            //topSellingModel.setChoice_options(choice_options);
                            // topSellingModel.setColors(colors);
                            //topSellingModel.setVariations(variations);
                            topSellingModel.setTodays_deal(todays_deal);
                            topSellingModel.setPublished(published);
                            topSellingModel.setFeatured(featured);
                            topSellingModel.setCurrent_stock(current_stock);
                            topSellingModel.setUnit(unit);
                            topSellingModel.setDiscount(discount);
                            topSellingModel.setDiscount_type(discount_type);
                            topSellingModel.setTax(tax);
                            topSellingModel.setTax_type(tax_type);
                            topSellingModel.setShipping_type(shipping_type);
                            topSellingModel.setShipping_cost(shipping_cost);
                            topSellingModel.setWeight(weight);
                            topSellingModel.setParcel_size(parcel_size);
                            topSellingModel.setNum_of_sale(num_of_sale);
                            topSellingModel.setMeta_title(meta_title);
                            topSellingModel.setMeta_description(meta_description);
                            topSellingModel.setMeta_img(meta_img);
                            topSellingModel.setPdf(pdf);
                            topSellingModel.setSlug(slug);
                            topSellingModel.setRating(rating);
                            topSellingModel.setCreated_at(created_at);
                            topSellingModel.setUpdated_at(updated_at);

                            topSellingArrayList.add(topSellingModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false);
                        topSellingRecyclerView.setLayoutManager(layoutManager);

                        topSellingPerSellerAdapter = new TopSellingPerSellerAdapter(context, topSellingArrayList);
                        topSellingPerSellerAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {

                            }
                        });
                        topSellingRecyclerView.setAdapter(topSellingPerSellerAdapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    private void loadRelatedProducts(int categories_ids){
        relatedProductsArrayList = new ArrayList<>();
        relatedProdRecyclerView = findViewById(R.id.relatedProdRecyclerView);

        RequestParams params = new RequestParams();
        params.put("category_id", categories_ids);
        Debugger.logD("category_id "+ categories_ids);
        HttpProvider.post(context, "/mobile/relatedProducts", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody, StandardCharsets.UTF_8);
                if (str.equals("[]")){
                }else {
                    try {
                        JSONArray jsonArray = new JSONArray(str);
                        for (int i = 0; i <= jsonArray.length() - 1; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String added_by = jsonObject.getString("added_by");
                            String user_id = jsonObject.getString("user_id");
                            String category_id = jsonObject.getString("category_id");
                            String subcategory_id = jsonObject.getString("subcategory_id");
                            String subsubcategory_id = jsonObject.getString("subsubcategory_id");
                            String brand_id = jsonObject.getString("brand_id");
                            String photos = jsonObject.getString("photos");
                            String thumbnail_img = jsonObject.getString("thumbnail_img");
                            String featured_img = jsonObject.getString("featured_img");
                            String flash_deal_img = jsonObject.getString("flash_deal_img");
                            String video_provider = jsonObject.getString("video_provider");
                            String video_link = jsonObject.getString("video_link");
                            String tags = jsonObject.getString("tags");
                            String description = jsonObject.getString("description");
                            String unit_price = jsonObject.getString("unit_price");
                            String purchase_price = jsonObject.getString("purchase_price");
                            String choice_options = jsonObject.getString("choice_options");
                            String colors = jsonObject.getString("colors");
                            String variations = jsonObject.getString("variations");
                            String todays_deal = jsonObject.getString("todays_deal");
                            String published = jsonObject.getString("published");
                            String featured = jsonObject.getString("featured");
                            String current_stock = jsonObject.getString("current_stock");
                            String unit = jsonObject.getString("unit");
                            String discount = jsonObject.getString("discount");
                            String discount_type = jsonObject.getString("discount_type");
                            String tax = jsonObject.getString("tax");
                            String tax_type = jsonObject.getString("tax_type");
                            String shipping_type = jsonObject.getString("shipping_type");
                            String shipping_cost = jsonObject.getString("shipping_cost");
                            String weight = jsonObject.getString("weight");
                            String parcel_size = jsonObject.getString("parcel_size");
                            String num_of_sale = jsonObject.getString("num_of_sale");
                            String meta_title = jsonObject.getString("meta_title");
                            String meta_description = jsonObject.getString("meta_description");
                            String meta_img = jsonObject.getString("meta_img");
                            String pdf = jsonObject.getString("pdf");
                            String slug = jsonObject.getString("slug");
                            String rating = jsonObject.getString("rating");
                            String created_at = jsonObject.getString("created_at");
                            String updated_at = jsonObject.getString("updated_at");

                            RelatedProducts relatedProductsModel = new RelatedProducts();

                            relatedProdPhotosArrayList = new ArrayList<>();
                            JSONArray jsonArrays = new JSONArray(Arrays.asList(photos));

                            for(int z=0; z<jsonArrays.length(); z++) {
                                JSONObject jobject = jsonArray.getJSONObject(z);

                                String photos2 = jobject.getString("photos");

                                String result = photos2.substring(2, photos2.length() -2).replace("\\","");

                                RelatedProducts.Photos photos1 = new RelatedProducts.Photos();
                                photos1.setPhoto(result);

                                relatedProdPhotosArrayList.add(photos1);

                            }

                            relatedProductsModel.setId(id);
                            relatedProductsModel.setName(name);
                            relatedProductsModel.setAdded_by(added_by);
                            relatedProductsModel.setUser_id(user_id);
                            relatedProductsModel.setCategory_id(category_id);
                            relatedProductsModel.setSubcategory_id(subcategory_id);
                            relatedProductsModel.setSubsubcategory_id(subsubcategory_id);
                            relatedProductsModel.setBrand_id(brand_id);
                            //relatedProductsModel.setPhotos(photos);
                            relatedProductsModel.setThumbnail_img(thumbnail_img);
                            relatedProductsModel.setFeatured_img(featured_img);
                            relatedProductsModel.setFlash_deal_img(flash_deal_img);
                            relatedProductsModel.setVideo_provider(video_provider);
                            relatedProductsModel.setVideo_link(video_link);
                            relatedProductsModel.setTags(tags);
                            relatedProductsModel.setDescription(description);
                            relatedProductsModel.setUnit_price(unit_price);
                            relatedProductsModel.setPurchase_price(purchase_price);
                            //relatedProductsModel.setChoice_options(choice_options);
                            // relatedProductsModel.setColors(colors);
                            //relatedProductsModel.setVariations(variations);
                            relatedProductsModel.setTodays_deal(todays_deal);
                            relatedProductsModel.setPublished(published);
                            relatedProductsModel.setFeatured(featured);
                            relatedProductsModel.setCurrent_stock(current_stock);
                            relatedProductsModel.setUnit(unit);
                            relatedProductsModel.setDiscount(discount);
                            relatedProductsModel.setDiscount_type(discount_type);
                            relatedProductsModel.setTax(tax);
                            relatedProductsModel.setTax_type(tax_type);
                            relatedProductsModel.setShipping_type(shipping_type);
                            relatedProductsModel.setShipping_cost(shipping_cost);
                            relatedProductsModel.setWeight(weight);
                            relatedProductsModel.setParcel_size(parcel_size);
                            relatedProductsModel.setNum_of_sale(num_of_sale);
                            relatedProductsModel.setMeta_title(meta_title);
                            relatedProductsModel.setMeta_description(meta_description);
                            relatedProductsModel.setMeta_img(meta_img);
                            relatedProductsModel.setPdf(pdf);
                            relatedProductsModel.setSlug(slug);
                            relatedProductsModel.setRating(rating);
                            relatedProductsModel.setCreated_at(created_at);
                            relatedProductsModel.setUpdated_at(updated_at);

                            relatedProductsArrayList.add(relatedProductsModel);
                        }
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
                        relatedProdRecyclerView.setLayoutManager(layoutManager);

                        relatedProductsAdapter = new RelatedProductsAdapter(context, relatedProductsArrayList);
                        relatedProductsAdapter.setClickListener(new OnClickRecyclerView() {
                            @Override
                            public void onItemClick(View view, int position) {

                            }
                        });
                        relatedProdRecyclerView.setAdapter(relatedProductsAdapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                        Debugger.logD("shit " + e.toString());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }



}
