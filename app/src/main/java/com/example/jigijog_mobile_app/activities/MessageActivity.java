package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.MessagesAdapter;
import com.example.jigijog_mobile_app.fragments.ChatFragment;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Messages;
import com.example.jigijog_mobile_app.utils.Tools;

import java.util.ArrayList;

import static com.example.jigijog_mobile_app.fragments.MessagesFragment.DIALOG_QUEST_CODE;

public class MessageActivity extends AppCompatActivity {
    private int request_code = 0;
    private View view;
    private Context context;

    private ImageView iv_chats, iv_notifications,iv_messageBack;

    private RecyclerView messagesRecyclerView;
    private ArrayList<Messages> messagesArrayList;
    private MessagesAdapter messagesAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        Tools.setSystemBarColor(this   , R.color.deep_orange_400);
        Tools.setSystemBarLight(this);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        initailizeUI();
        loadAllMessages();
    }

    private void initailizeUI() {
        iv_messageBack = findViewById(R.id.iv_messageBack);
        iv_chats = findViewById(R.id.iv_chats);
        iv_notifications = findViewById(R.id.iv_notifications);
        iv_chats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChatActivity.class);
                startActivity(intent);
            }
        });
        iv_notifications.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChatActivity.class);
                startActivity(intent);
            }
        });

        iv_messageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void loadAllMessages() {
        messagesRecyclerView = findViewById(R.id.messagesRecyclerView);
        messagesArrayList = new ArrayList<>();

        messagesArrayList.add(new Messages(R.drawable.ic_stars_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider1,"Guess the Price, Win the Price from 6-10PM today! \n Click to learn more."));
        messagesArrayList.add(new Messages(R.drawable.ic_stars_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider2,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));
        messagesArrayList.add(new Messages(R.drawable.ic_notifications_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider4,"Guess the Price, Win the Price from 6-10PM today! \nClick to learn more."));
        messagesArrayList.add(new Messages(R.drawable.ic_check_box_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider7,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        messagesRecyclerView.setLayoutManager(layoutManager);

        messagesAdapter = new MessagesAdapter(context, messagesArrayList);
        messagesAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
        messagesRecyclerView.setAdapter(messagesAdapter);
    }
}
