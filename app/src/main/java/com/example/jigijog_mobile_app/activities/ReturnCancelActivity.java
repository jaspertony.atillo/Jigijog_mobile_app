package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.ChatViewPagerAdapter;
import com.example.jigijog_mobile_app.fragments.ToPayFragment;
import com.example.jigijog_mobile_app.fragments.ToReceiveFragment;
import com.example.jigijog_mobile_app.fragments.ToReviewFragment;
import com.example.jigijog_mobile_app.fragments.ToShipFragment;
import com.example.jigijog_mobile_app.utils.Tools;
import com.google.android.material.tabs.TabLayout;

public class ReturnCancelActivity extends AppCompatActivity {
    private View view;
    private Context context;

    private ImageView iv_myReturnBack;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ChatViewPagerAdapter viewPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_cancel);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Tools.setSystemBarColor(this, R.color.deep_orange_400);
        Tools.setSystemBarLight(this);
        initializeUI();
    }

    private void initializeUI () {
        iv_myReturnBack = findViewById(R.id.iv_myReturnBack);
        iv_myReturnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.view_pager);
        viewPagerAdapter = new ChatViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new ToPayFragment(), "Return");
        viewPagerAdapter.addFragment(new ToShipFragment(), "Cancellation");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_assignment_return_black_24dp);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_cancel_black_24dp);

        tabLayout.getTabAt(0).getIcon().setTint(getResources().getColor(R.color.colorAccent,getTheme()));
        tabLayout.getTabAt(1).getIcon().setTint(getResources().getColor(R.color.colorAccent,getTheme()));
        //createTabIcons();
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getIcon().setTint(getResources().getColor(R.color.deep_orange_400,getTheme()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setTint(getResources().getColor(R.color.colorAccent,getTheme()));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
