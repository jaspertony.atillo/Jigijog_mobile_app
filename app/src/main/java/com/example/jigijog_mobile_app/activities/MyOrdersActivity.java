package com.example.jigijog_mobile_app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.ChatViewPagerAdapter;
import com.example.jigijog_mobile_app.fragments.CancellationFragment;
import com.example.jigijog_mobile_app.fragments.ChatFragment;
import com.example.jigijog_mobile_app.fragments.NotificationFragment;
import com.example.jigijog_mobile_app.fragments.ReturnFragment;
import com.example.jigijog_mobile_app.fragments.ToPayFragment;
import com.example.jigijog_mobile_app.fragments.ToReceiveFragment;
import com.example.jigijog_mobile_app.fragments.ToReviewFragment;
import com.example.jigijog_mobile_app.fragments.ToShipFragment;
import com.example.jigijog_mobile_app.utils.Tools;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

public class MyOrdersActivity extends AppCompatActivity {
    private View view;
    private Context context;
    private ImageView iv_myOrdersBack;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ChatViewPagerAdapter viewPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
        context = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Tools.setSystemBarColor(this, R.color.deep_orange_400);
        Tools.setSystemBarLight(this);
        initializeUI();

    }

    private void initializeUI (){
        iv_myOrdersBack = findViewById(R.id.iv_myOrdersBack);
        iv_myOrdersBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.view_pager);

        viewPagerAdapter = new ChatViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new ToPayFragment(), "To Pay");
        viewPagerAdapter.addFragment(new ToShipFragment(), "To Ship");
        viewPagerAdapter.addFragment(new ToReceiveFragment(), "To Receive");
        viewPagerAdapter.addFragment(new ToReviewFragment(), "To Review");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_payment_black_24dp);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_book_black_24dp);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_local_shipping_black_24dp);
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_rate_review_black_24dp);

        tabLayout.getTabAt(0).getIcon().setTint(getResources().getColor(R.color.colorAccent,getTheme()));
        tabLayout.getTabAt(1).getIcon().setTint(getResources().getColor(R.color.colorAccent,getTheme()));
        tabLayout.getTabAt(2).getIcon().setTint(getResources().getColor(R.color.colorAccent,getTheme()));
        tabLayout.getTabAt(3).getIcon().setTint(getResources().getColor(R.color.colorAccent,getTheme()));
        //createTabIcons();
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getIcon().setTint(getResources().getColor(R.color.deep_orange_400,getTheme()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setTint(getResources().getColor(R.color.colorAccent,getTheme()));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    private void createTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("To Pay");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_chat_black_24dp, 0, 0);
        Objects.requireNonNull(tabLayout.getTabAt(0)).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("To Receive");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_notifications_black_24dp, 0, 0);
        Objects.requireNonNull(tabLayout.getTabAt(1)).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText("To Receive");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_chat_black_24dp, 0, 0);
        Objects.requireNonNull(tabLayout.getTabAt(2)).setCustomView(tabThree);

        TextView tabFour = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFour.setText("To Review");
        tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_notifications_black_24dp, 0, 0);
        Objects.requireNonNull(tabLayout.getTabAt(3)).setCustomView(tabFour);


    }
}
