package com.example.jigijog_mobile_app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.jigijog_mobile_app.activities.StartActivity;
import com.example.jigijog_mobile_app.models.UserCustomer;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.ProgressPopup;
import com.example.jigijog_mobile_app.utils.UserRole;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class MainActivity extends AppCompatActivity {
    private Context context;
    private String role;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_feed, R.id.navigation_messages,R.id.navigation_cart, R.id.navigation_account)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        //NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        context = this;
        role = UserRole.getRole(context);
        Debugger.logD("rrr " + role);
        checkUserRole();
        //checkCustomerSession();
    }

    private void checkUserRole() {
        if (!role.isEmpty()) {
            checkCustomerSession();
        }else{

        }

    }


    public void checkCustomerSession() {
//        if (!UserRole.getRole(context).isEmpty()) {
//            loginUser();
//        }else{
//
//        }
        String userToken = UserCustomer.getID(context);
        Debugger.logD("userToken " + userToken);
        if(userToken.equals("")){

        }else{
            UserRole userType = new UserRole();
            userType.setUserRole(UserRole.Customer());
            userType.saveRole(context);

        }


    }

    private void loginUser() {
        ProgressPopup.showProgress(context);

        RequestParams params = new RequestParams();
        params.put("username", UserCustomer.getEmail(context));
        params.put("password", UserCustomer.getPassword(context));

        HttpProvider.post(context, "/mobile/loginUser", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ProgressPopup.hideProgress();
                try {
                    String str = new String(responseBody, StandardCharsets.UTF_8);
                    JSONArray jsonArray = new JSONArray(str);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String result = jsonObject.getString("result");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }


}
