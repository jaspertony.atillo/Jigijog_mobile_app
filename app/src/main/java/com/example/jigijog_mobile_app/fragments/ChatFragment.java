package com.example.jigijog_mobile_app.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.ChatsAdapter;
import com.example.jigijog_mobile_app.adapters.NotificationsAdapter;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Chats;
import com.example.jigijog_mobile_app.models.Notifications;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends DialogFragment {
    private int request_code = 0;
    private View view;
    private Context context;
    private RecyclerView chatRecyclerView;
    private ArrayList<Chats> chatsArrayList;
    private ChatsAdapter chatsAdapter;
    public ChatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chat, container, false);
        context = getContext();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeUI();
    }

    private void initializeUI () {
        chatRecyclerView = view.findViewById(R.id.chatRecyclerView);

        chatsArrayList = new ArrayList<>();

        chatsArrayList.add(new Chats(R.drawable.ic_chat_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider1,"Guess the Price, Win the Price from 6-10PM today! \n Click to learn more."));
        chatsArrayList.add(new Chats(R.drawable.ic_chat_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider2,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));
        chatsArrayList.add(new Chats(R.drawable.ic_chat_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider4,"Guess the Price, Win the Price from 6-10PM today! \nClick to learn more."));
        chatsArrayList.add(new Chats(R.drawable.ic_chat_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider7,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        chatRecyclerView.setLayoutManager(layoutManager);

        chatsAdapter = new ChatsAdapter(context, chatsArrayList);
        chatsAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
        chatRecyclerView.setAdapter(chatsAdapter);
    }
    public void setRequestCode(int request_code) {
        this.request_code = request_code;
    }

}
