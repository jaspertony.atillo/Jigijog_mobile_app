package com.example.jigijog_mobile_app.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.adapters.MessagesAdapter;
import com.example.jigijog_mobile_app.adapters.NotificationsAdapter;
import com.example.jigijog_mobile_app.interfaces.OnClickRecyclerView;
import com.example.jigijog_mobile_app.models.Messages;
import com.example.jigijog_mobile_app.models.Notifications;

import java.util.ArrayList;


public class NotificationFragment extends DialogFragment {
    private int request_code = 0;
    private View view;
    private Context context;

    private RecyclerView notificationRecyclerView;
    private ArrayList<Notifications> notificationsArrayList;
    private NotificationsAdapter notificationsAdapter;
    public NotificationFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_notification, container, false);
        context = getContext();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeUI();
    }

    private void initializeUI () {
        notificationRecyclerView = view.findViewById(R.id.notificationRecyclerView);

        notificationsArrayList = new ArrayList<>();

        notificationsArrayList.add(new Notifications(R.drawable.ic_notifications_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider1,"Guess the Price, Win the Price from 6-10PM today! \n Click to learn more."));
        notificationsArrayList.add(new Notifications(R.drawable.ic_notifications_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider2,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));
        notificationsArrayList.add(new Notifications(R.drawable.ic_notifications_black_24dp, "Over 2 Million worth of prizes to be won!","09:02AM",R.drawable.slider4,"Guess the Price, Win the Price from 6-10PM today! \nClick to learn more."));
        notificationsArrayList.add(new Notifications(R.drawable.ic_notifications_black_24dp, "Win 111,111 credits with just 1!","Yesterday",R.drawable.slider7,"Join LUCKY PISO now and use Lazada Wallet! \n Round ends on Nov 11 23:59. Draw on Nov 12."));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        notificationRecyclerView.setLayoutManager(layoutManager);

        notificationsAdapter = new NotificationsAdapter(context, notificationsArrayList);
        notificationsAdapter.setClickListener(new OnClickRecyclerView() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
        notificationRecyclerView.setAdapter(notificationsAdapter);

    }
    public void setRequestCode(int request_code) {
        this.request_code = request_code;
    }

}
