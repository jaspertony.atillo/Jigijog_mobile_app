package com.example.jigijog_mobile_app.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.fragment.app.Fragment;

import com.example.jigijog_mobile_app.HttpProvider;
import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.activities.LoginActivity;
import com.example.jigijog_mobile_app.activities.MyOrdersActivity;
import com.example.jigijog_mobile_app.activities.ReturnCancelActivity;
import com.example.jigijog_mobile_app.activities.SettingsActivity;
import com.example.jigijog_mobile_app.activities.StartActivity;
import com.example.jigijog_mobile_app.models.UserCustomer;
import com.example.jigijog_mobile_app.utils.Debugger;
import com.example.jigijog_mobile_app.utils.ProgressPopup;
import com.example.jigijog_mobile_app.utils.Tools;
import com.example.jigijog_mobile_app.utils.UserRole;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;


public class AccountFragment extends Fragment {

    private View view;
    private Context context;

    private ImageView iv_accountSetting,iv_accountReturns,iv_accountCancel;
    private TextView tv_accountName,tv_myorders;

    private UserCustomer userCustomer = new UserCustomer();

    private String role;

    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_account, container, false);
        context = getContext();
        role = UserRole.getRole(context);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Tools.setSystemBarColor(getActivity(), R.color.deep_orange_400);
        Tools.setSystemBarLight(getActivity());
        initializeUI();
        checkUserRole();
        //checkCustomerSession();
    }

    private void initializeUI () {
        iv_accountSetting = view.findViewById(R.id.iv_accountSetting);
        tv_accountName = view.findViewById(R.id.tv_accountName);
        tv_myorders = view.findViewById(R.id.tv_myorders);
        iv_accountReturns = view.findViewById(R.id.iv_accountReturns);
        iv_accountCancel = view.findViewById(R.id.iv_accountCancel);
        iv_accountReturns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ReturnCancelActivity.class);
                startActivity(intent);
            }
        });
        iv_accountCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ReturnCancelActivity.class);
                startActivity(intent);
            }
        });
        tv_myorders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MyOrdersActivity.class);
                startActivity(intent);
            }
        });
        iv_accountSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SettingsActivity.class);
                startActivity(intent);
            }
        });
        tv_accountName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void checkUserRole(){
        if (role.equals(UserRole.Customer())) {
            checkCustomerSession();
        } else {
            //checkStudentSession();
        }
    }

    public void checkCustomerSession() {
//        if (!UserRole.getRole(context).isEmpty()) {
//            loginUser();
//        }else{
//
//        }
        String userToken = UserCustomer.getID(context);
        Debugger.logD("rt" + userToken);

        if(!userToken.equals("")){
            //loginUser();
            tv_accountName.setText(UserCustomer.getName(context));
            Debugger.logD("fuck " + UserCustomer.getName(context) + UserCustomer.getEmail(context));
        }else{
            loginUser();
            UserRole userType = new UserRole();
            userType.setUserRole(UserRole.Customer());
            userType.saveRole(context);

        }


    }

    private void loginUser() {
        ProgressPopup.showProgress(context);

        RequestParams params = new RequestParams();
        params.put("username", UserCustomer.getEmail(context));
        params.put("password", UserCustomer.getPassword(context));

        HttpProvider.post(context, "/mobile/loginUser", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ProgressPopup.hideProgress();
                try {
                    String str = new String(responseBody, StandardCharsets.UTF_8);
                    JSONArray jsonArray = new JSONArray(str);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String id = jsonObject.getString("id");
                    String provider_id = jsonObject.getString("provider_id");
                    String user_type = jsonObject.getString("user_type");
                    String name = jsonObject.getString("name");
                    String email = jsonObject.getString("email");

                    tv_accountName.setText(email);
                    Debugger.logD(name + email);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                ProgressPopup.hideProgress();
                Toasty.error(context, "No internet connect. Please try again.").show();
            }
        });
    }




}
