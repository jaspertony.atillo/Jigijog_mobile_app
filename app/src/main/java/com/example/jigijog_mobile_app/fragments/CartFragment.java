package com.example.jigijog_mobile_app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.jigijog_mobile_app.R;
import com.example.jigijog_mobile_app.utils.Tools;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;


public class CartFragment extends Fragment {

    private View view;
    private Context context;

    private Spinner spinner;
    private Button btn_applyVoucher;
    private TextView tv_mycartDelete;
    public CartFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_cart, container, false);
        context = getContext();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Tools.setSystemBarColor(getActivity(), R.color.deep_orange_400);
        Tools.setSystemBarLight(getActivity());
        initializeUI();
    }

    private void initializeUI() {
        btn_applyVoucher = view.findViewById(R.id.btn_applyVoucher);
        tv_mycartDelete = view.findViewById(R.id.tv_mycartDelete);
        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("Color: Red");
        spinnerArray.add("Color: Black");
        spinnerArray.add("Color: Pink");
        spinnerArray.add("Color: Green");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                context, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner = view.findViewById(R.id.spinner);
        spinner.setAdapter(adapter);
        btn_applyVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.warning(context, "Button Voucher code Clicked").show();
            }
        });
        tv_mycartDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toasty.warning(context, "Delete Cart Function Clicked").show();
            }
        });
    }


}
